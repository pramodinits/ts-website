<?php include 'header.php'; ?>
    <?php include 'inner-nav.php'; ?>
    <div class="clearfix"></div>
    <div class="tp-bgimg" id="wt-we-are">
        <div class="container-fluid">
            <div class="row fill-main">
                <div class="col-lg-6 text-center">
<!--                    <h1 class="p-5">TS <span class="text-green">TALE</span></p></h1>-->
                    <h3 class="wow fadeInUp text-uppercase" data-wow-delay="400ms" style="font-weight: 900; visibility: visible; animation-delay: 400ms; animation-name: fadeInUp;">TS TALE</h3>
                    <div class="text-center">
                    <img class="img-fluid" src="images/gradient_news.png"/>
                </div>
                    <h5 class="text-center mt-4 wow fadeInUp" data-wow-delay="300ms" style="visibility: visible; animation-delay: 300ms; animation-name: fadeInUp;"><b>We develop apps and web platform and maintain the same!</b> </h5>
                </div>
                <div class="col-lg-6 " style="background-color: #00d2b2 !important; opacity: .8;">
                    <p class="text-white text-justify" style="padding: 40% 10%; color: #2b282a !important;">Going Digital is pre-eminent not only for enterprise success but also to save the planet. We want to collaborate with you in taking this initiative forward. We facilitate the process of business transformation through automation adoption. That's the best way to meet the entrepreneurial challenges arising in the current business ecosystem. Execution of your IT requirement gets as easy as it could with TS around.</p></div>
            </div>
        </div>
    </div>

    <div class="tp-bgimg defaultimg " data-bgcolor="undefined" style='background-repeat: no-repeat; background-image: url( "images/aboout_banner.jpg"); background-size: cover; background-position: center center; width: 100%; height: 100%; opacity: 1; visibility: inherit; z-index: 20;' src="http://www.themesindustry.com/html/weone-2019/images/arrow-slide1.jpg">
        
    </div>
    <div id="mission">
    <section class="fill-main">
        <div class="container">
            <div class="row align-items-center">
                <div class="col-lg-4 ">
                    <div class="img-hvr">
                        <img src="images/mission.jpg" alt="">
                    </div>
                </div>
                <div class="col-lg-8 ">
                    <h3 class="wow fadeInUp" data-wow-delay="300" style="visibility: visible; animation-name: fadeInUp;">Mission
                    </h3>
                    <div class="border border-success"></div>
                    <h5 class="mt-3 wow fadeInUp font-weight-bold" data-wow-delay="300ms" style="visibility: visible; animation-delay: 300ms; animation-name: fadeInUp;">Our Focus</h5>
                    <p class="mt-2 wow fadeInUp" data-wow-delay="400" style="visibility: visible; animation-name: fadeInUp;">Leverage our skills in a collaborative manner to continually enhance our products and services to exceed customer expectations..</p>
                </div>
            </div>
        </div>
    </section>
    <section class="fill-main">
        <div class="container">
            <div class="row align-items-center">
                <div class="col-lg-4 order-lg-1">
                    <div class="img-hvr text-right">
                        <img src="images/vision.jpg" class="mw-100" alt="">
                    </div>
                </div>
                <div class="col-lg-8 ">
                    <h3 class="wow fadeInUp" data-wow-delay="300" style="visibility: visible; animation-name: fadeInUp;">Vision
                    </h3>
                    <div class="border border-success"></div>
                    <h5 class="mt-3 wow fadeInUp font-weight-bold" data-wow-delay="300ms" style="visibility: visible; animation-delay: 300ms; animation-name: fadeInUp;">Dominating Digital Space</h5>
                    <p class="mt-2 wow fadeInUp" data-wow-delay="400" style="visibility: visible; animation-name: fadeInUp;">To become an excellent hub of system design and technical competence.</p>
                </div>
            </div>
        </div>
    </section>
    <section class="fill-main ">
        <div class="container ">
            <div class="row align-items-center">
                <div class="col-lg-4">
                    <div class="img-hvr text-left">
                        <img src="images/values.jpg" alt="">
                    </div>
                </div>
                <div class="col-lg-8 ">
                    <h3 class="wow fadeInUp" data-wow-delay="300" style="visibility: visible; animation-name: fadeInUp;">Values </h3>
                    <div class="border border-success"></div>
                    <h5 class="mt-3 wow fadeInUp font-weight-bold" data-wow-delay="300ms" style="visibility: visible; animation-delay: 300ms; animation-name: fadeInUp;">Customer Centricity</h5>
                    <p class="mt-2 wow fadeInUp" data-wow-delay="400" style="visibility: visible; animation-name: fadeInUp;">Customer is at the core either – External or Internal. We deliver measurable outcome against customer's investment and strive for excellence holistically.</p>
                    <h5 class="mt-3 wow fadeInUp font-weight-bold" data-wow-delay="300ms" style="visibility: visible; animation-delay: 300ms; animation-name: fadeInUp;">Innovation</h5>
                    <p class="mt-2 wow fadeInUp" data-wow-delay="400" style="visibility: visible; animation-name: fadeInUp;">Our forte is acquisition and implementation of new capabilities which allows competitive edge thus assuring leadership in the arena.</p>
                    <h5 class="mt-3 wow fadeInUp font-weight-bold" data-wow-delay="300ms" style="visibility: visible; animation-delay: 300ms; animation-name: fadeInUp;">Respect & Trust</h5>
                    <p class="mt-2 wow fadeInUp" data-wow-delay="400" style="visibility: visible; animation-name: fadeInUp;">We appreciate diversity, respect individuality and encourage cooperation to nurture the shared vision.</p>
                    <h5 class="mt-3 wow fadeInUp font-weight-bold" data-wow-delay="300ms" style="visibility: visible; animation-delay: 300ms; animation-name: fadeInUp;">Professionalism</h5>
                    <p class="mt-2 wow fadeInUp" data-wow-delay="400" style="visibility: visible; animation-name: fadeInUp;">Being a value driven organization, integrity and professionalism assume paramount importance in our business operations.</p>
                </div>
            </div>
        </div>
    </section>
</div>
    <div class="mt-team" id="our-team">
            <div class="fill-main">
                <div class="container">
                    <div>
                        <h3 class="wow fadeInUp text-center text-white" data-wow-delay="300" style="visibility: visible; animation-name: fadeInUp;">MEET OUR <span style="color: #15496e;">TEAM</span> </h3>
                    </div>
                    <div class="clearfix"></div>
                    <div class="border-ctm border-light"></div>
                    <div class="col-md-12 client">
                        <div class="mt-4"> </div>
                        <p class="font-weight-light text-white text-justify" style="font-size:18px;"> <i class="fa fa-quote-left text-left pr-2"></i>
                            It is group of dynamic professionals who bring to table a rich industry experience.Keeping their team motivated with a challenging environment and acknowledgement of their potential has manifested itself in the shape of numerous innovative products and solutions resulting in a long list of pleased clientele. Highly talented and skilled in their respective domain they guide the team effortlessly thus eliminating the scope of any work pressure hence ensuring a smooth work life balance. A management team which boasts to have a bunch of happy geeks in their team too excited to be a part of the company!<i class="fa fa-quote-right text-right pl-2"></i></p>
                    </div>
                </div>
            </div>
    </div>
    <!-- address Start -->
    <?php include 'footer.php'; ?>
