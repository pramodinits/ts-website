<!DOCTYPE html>
<html lang="en">
    <head>
        
      
      
        <!-- Modernizr js - required -->
      
        
        <!-- Required meta tags -->
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
        <title>thoughtspheres</title>    
        <?php include 'header.php';?>
    </head>
    <body data-spy="scroll" data-target=".navbar" data-offset="90">
        <!--loader start-->
        <!--loader end-->

        <!--header start-->

         <?php include 'inner-nav.php';?>
        <div style="background-color:#fffff;" >
            <div class="container" >
                <div class="row ">
                    <div class="col-lg-6 mt-2 " style="padding-top:100px;">
                        <h3 class="wow fadeInUp text-black" data-wow-delay="300" style="visibility: visible; animation-name: fadeInUp;">Web Platform </h3>
                        <div class="border border-success text-white"></div>
                        <h5 class="mt-3 wow fadeInUp" data-wow-delay="300ms" style="visibility: visible; animation-delay: 300ms; animation-name: fadeInUp;"><p class="text-justify">In the current digital world, websites are inevitable requirement for any organization.They create the first impression for the local and global clients thus have extended beyond few static templates. Businesses are looking for comprehensive websites which sometimes behaves like a transactional web application as well. Thoughtspheres (TS) is offering its expertise in designing and developing secure, reliable and scalable websites by understanding the customer’s nature of business and requirement. TS boasts of dealing with quite a few prestigious projects for Govt.,MNCs etc.</p>
Enterprise Development:</h5>
                        <ul class="pl-3 wow fadeInUp" data-wow-delay="600ms" style="visibility: visible; animation-delay: 600ms; animation-name: fadeInUp;">
                    <li>Content Management: WordPress,Drupal,Joomla</li>
                    <li> Ecommerce: Magento, Joomla</li>
                    <li>Front End: HTML5, CSS3, JavaScript, AngularJS, AJAX, JQuery, Bootstrap</li>
                    
                     <li>Security: SSL, Encryption</li>
                    <li>Scripting Languages: Ruby on Rails , Python</li>
                    <li> Frameworks: CodeIgniter,Yii,CakePHP,Laravel,Zend</li>                   
                                 
                </ul> 

                 </div>
                    <div class="col-lg-6 mt-5 pt-5 pb-5">
                        <img src="images/tech-mobility.png" class="img-fluid" style="margin-top: 30%;">
                    </div>
                </div>
            </div>
        </div>
        <!--header end-->
        <div class="clearfix"></div>
        <?php include 'ourclient.php'; ?>
            <!-- address Start -->
            <!-- Footer end -->
           <?php include 'footer.php';?>
    </body>
</html>