<!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
        <title>thoughtspheres</title>    
        <?php include 'header.php'; ?>
    </head>
    <body data-spy="scroll" data-target=".navbar" data-offset="90">

        <?php include 'inner-nav.php'; ?>
        <div style="background-color:#fffff;" >
            <div class="container" >
                <div class="row ">
                    <div class="col-lg-6 mt-2 " style="padding-top:100px;">
                        <h3 class="wow fadeInUp text-black" data-wow-delay="300" style="visibility: visible; animation-name: fadeInUp;">IOT </h3>
                        <div class="border border-success text-white"></div>
                        <h5 class="mt-3 wow fadeInUp" data-wow-delay="300ms" style="visibility: visible; animation-delay: 300ms; animation-name: fadeInUp;"><p class="text-justify">IoT,the latest buzz of the internet world, connects smart objects to the Internet. The IoT is soon going to serve as a transformational tool which would turn a large number of enterprises into digital businesses.IoT shall act as a breeding ground for new business models,would ensure enhanced productivity and shall lead to creation of new modes of revenue generation.According to a study by Gartner, by 2020 , 25 billion Internet-connected things will to lead to generation of $2 trillion of economic benefit globally. ThoughtSpheres possess a thoroughly equipped and enthusiastic team for this most current segment thus proudly aspires to be competent carrier of such extraordinary growth which is sure to be witnessed by the IT world via IoT.</p>
                        </h5>
                    </div>
                    <div class="col-lg-6 mt-5 pt-5 pb-5">
                        <img src="images/tech-mobility.png" class="img-fluid" style="margin-top: 30%;">
                    </div>
                </div>
            </div>
        </div>
        <div class="clearfix"></div>
        <?php include 'ourclient.php'; ?>
        <?php include 'footer.php'; ?>
    </body>
</html>