<!DOCTYPE html>
<html lang="en">
    <head>
    <!-- Required meta tags -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <title>Thoughtspheres</title>
    <!-- Bootstrap CSS -->
    <!-- REVOLUTION STYLE SHEETS -->
    <link rel="stylesheet" href="css/jquery.fancybox.min.css">
    <link rel="stylesheet" href="css/owl.carousel.css">
    <link rel="stylesheet" href="css/bootstrap.min.css">
    <link rel="stylesheet" href="css/animate.css">
    <link rel="stylesheet" href="css/main.css"> 
    <link rel="stylesheet" href="css/style.css">
    <link rel="stylesheet" href="css/responsive.css">
    <link rel="stylesheet" href="css/font-awesome.min.css">
    <link rel="stylesheet" href="css/jquery-ui.css">     
    
    <script src="js/jquery-3.1.1.min.js"></script>
    <script src="js/modernizr.custom.js"></script>
    <script src="js/modernizr.min.js"></script>   
    <script src="js/jquery-ui.js"></script>
    <style>
    .ui-menu .ui-menu-item {
        padding: 10px 14px;
        font-weight: bold;
        letter-spacing: 1;
    }
    .ui-widget .ui-widget {
        width: 100%;
    }
    button.navbar-toggler i {
        color: #1c2a63;
    }
</style>
</head>
