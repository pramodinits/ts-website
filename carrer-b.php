<!DOCTYPE html>
<html lang="en">
    <head>      
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
        <title>thoughtspheres</title>    
        <?php include 'header.php'; ?>
    </head>

    <body data-spy="scroll" data-target=".navbar" data-offset="90">    
        <?php include 'inner-nav.php'; ?>       
        <div style="background-color:#fffff;" >
            <div class="container" >
                <section class="about" id="digital" >
                    <div class="row">
                        <div class="col-lg-12 mt-5 text-center">
                            <h2 class="wow fadeInUp text-black font-weight-bold" data-wow-delay="300" style="visibility: visible; animation-name: fadeInUp;">CAREERS</h2>
                            <div class="border-success text-white" style="border:solid 1px #28a745;"></div>
                            <h4 class="text-left mt-4 wow fadeInUp" data-wow-delay="300ms" style="visibility: visible; animation-delay: 300ms; animation-name: fadeInUp;"></h4>
                            <div class=" wow fadeInUp text-left mt-4" data-wow-delay="600ms" style="visibility: visible; animation-delay: 600ms; animation-name: fadeInUp;">
                                <h3 class="text-center">Be the architect of a smart digital space</h3>
                                <div class="card p-2 border-success mt-2">
                                    <div class="row ">                                  
                                        <div class="col-md-2 text-center ">
                                            <img src="images/whythoughts.png" class="img-fluid  mt-4">                                   
                                        </div>
                                        <div class="col-md-10 pt-4">
                                            <h5>Innovative Approach</h5>
                                            <p>Yes we do transform the business environment through disruptive technologies. Gulping in new capabilities and throwing out inventive solutions is a norm at TS.</p>
                                        </div> 
                                    </div>
                                    <div class="clearfix"></div>

                                </div>
                                <div class="card p-2 border-success mt-2">
                                    <div class="row ">                                  
                                        <div class="col-md-2 text-center">
                                            <img src="images/growthApportunity.png" class="img-fluid  mt-4">                                   
                                        </div>
                                        <div class="col-md-10 pt-4">
                                            <h5>Extraordinary Growth Opportunity</h5>
                                            <p>If you have the fire then we let you ignite (not literally though).Anybody with great potential is bound to take flight here. (Taking flights for successful employees is quite literally possible)</p>
                                        </div> 
                                    </div>
                                    <div class="clearfix"></div>

                                </div>
                                <div class="card p-2 border-success mt-2">
                                    <div class="row ">                                  
                                        <div class="col-md-2 text-center">
                                            <img src="images/excellentLeadership.png" class="img-fluid  mt-5">                                   
                                        </div>
                                        <div class="col-md-10 pt-4">
                                            <h5>Excellent Leadership Team</h5>
                                            <p>If you have the fire then we let you ignite (not literally though).Anybody with great potential is bound to take flight here. (Taking flightsWe have a strong and diverse leadership team who are but obviously exceptionally endowed; in addition to that they have a passion to foster the hidden talent as expected from true leaders.
                                            </p>
                                        </div> 
                                    </div>
                                    <div class="clearfix"></div>

                                </div>
                                <div class="card p-2 border-success mt-2">
                                    <div class="row ">                                  
                                        <div class="col-md-2 text-center">
                                            <img src="images/workEnvirnoment.png" class="img-fluid  mt-4">                                   
                                        </div>
                                        <div class="col-md-10 pt-4">
                                            <h5>Open & Transparen</h5>
                                            <p>At TS, we are open to feedback and transparent in our approach.So walk into our premises and you can expect a lot of open,peer to peer and associates to leadership team discussions. Can we say it is luck that yet we are able to deliver projects on schedule? ;)</p>
                                        </div> 
                                    </div>
                                    <div class="clearfix"></div>

                                </div>
                                <div class="card p-2 border-success mt-2">
                                    <div class="row ">                                  
                                        <div class="col-md-2 text-center">
                                            <img src="images/workLife.png" class="img-fluid  mt-4">                                   
                                        </div>
                                        <div class="col-md-10 pt-4">
                                            <h5>Work Life Balance</h5>
                                            <p>We certainly can’t afford to ignore work life balance in this ‘we live life only once’ era. ‘Smart Work’ is what we strongly advocate while work from home flexibility is provided as and when required. Thanks to our employee engagement team recreation activities sometimes delay our departure timings
                                            </p>
                                        </div> 
                                    </div>
                                    <div class="clearfix"></div>
                                </div>
                                <div class="card p-2 border-success mt-2">
                                    <div class="row ">                                  
                                        <div class="col-md-2 text-center" >
                                            <img src="images/employeementAppo.png" class="img-fluid mt-4" >                                   
                                        </div>
                                        <div class="col-md-10 pt-4">
                                            <h5>Equal Employment Opportunity</h5>
                                            <p>‘Equal Employment opportunity’ is the need of the hour and TS is bound to pitch in. We believe in skills and talent irrespective of the gender. Of course, the job aspirant/ employee is expected to prove one’s worth to grab the opportunities.
                                            </p>
                                        </div> 
                                    </div>
                                    <div class="clearfix"></div>

                                </div>
                                <div class="text-center">
                                    <small>We look for smart, Industrious, ambitious and self-starter people who will never shy away from responsibilities,send in your resume at: <span><a href="">resume@thoughtspheres.com</a</span></small>
                                </div>
                            </div>
                        </div>
                    </div>
                </section>                        
            </div>
            <!--header end-->
            <div class="clearfix"></div>            
       
        <?php include 'footer.php'; ?> 
             </div>
    </body>
</html>