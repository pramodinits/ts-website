<!DOCTYPE html>
<html lang="en">
    <head>
        
      
      
        <!-- Modernizr js - required -->
      
        
        <!-- Required meta tags -->
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
        <title>thoughtspheres</title>    
        <?php include 'header.php';?>
    </head>
    <body data-spy="scroll" data-target=".navbar" data-offset="90">
         <?php include 'inner-nav.php';?>
        <div style="background-color:#fffff;" >
            <div class="container" >
                <div class="row ">
                    <div class="col-lg-6 mt-2 " style="padding-top:100px;">
                        <h3 class="wow fadeInUp text-black" data-wow-delay="300" style="visibility: visible; animation-name: fadeInUp;">DW/BI Platform </h3>
                        <div class="border border-success text-white"></div>
                        <h5 class="mt-3 wow fadeInUp" data-wow-delay="300ms" style="visibility: visible; animation-delay: 300ms; animation-name: fadeInUp;"><p class="text-justify">A data warehouse/data mart represents historical data from different departments having different functions/operations across the domain in the corporate or enterprise. This is the process of extracting, integrating, aggregating, filtering, summarizing, cleansing, transforming and quality checking the organization applications data and storing it in a consolidated database. This database serves as the only source from which the management can access and retrieve information for significant decision-making.
Our DW and BI Group has experienced engineers, who have expertise in a range of DW and BI products and solutions, to come up with an optimal technical architecture and an implementation blue-print for their analytical and operational reporting requirements.</p>
Capabilities:</h5>
                        <ul class="pl-3 wow fadeInUp" data-wow-delay="600ms" style="visibility: visible; animation-delay: 600ms; animation-name: fadeInUp;">
                    <li> Architecture and Design (ETL strategy)</li>
                    <li>Data Modeling (OLTP and OLAP)</li>
                    <li> OLAP Reporting</li>
                     <li>Performance Tuning</li>
                    <li>DW Suppor</li>
                </ul> 
                        
                </div>
                    <div class="col-lg-6 mt-5 pt-5 pb-5">
                        <img src="images/tech-mobility.png" class="img-fluid" style="margin-top: 30%;">
                    </div>
                </div>
            </div>
        </div>
        <!--header end-->
        <div class="clearfix"></div>
        <?php include 'ourclient.php'; ?>   
            <!-- address Start -->
            <!-- Footer end -->
           <?php include 'footer.php';?>

    </body>


</html>