<?php include 'header.php'; ?>
<?php include 'inner-nav.php'; ?>
<div class="clearfix"></div>
<div class="tp-bgimg">
    <div class="container-fluid">
        <div class="row fill-main">
            <div class="col-lg-6 text-center">
                <h3 class="wow fadeInUp text-uppercase transparent-bghead" data-wow-delay="400ms">Enterprise <span class="text-ray">IT</span></h3>
                <div class="ctm-border"></div>
                <h5 class="text-center mt-4 wow fadeInUp" data-wow-delay="300ms" style="visibility: visible; animation-delay: 300ms; animation-name: fadeInUp;">
                    <strong>
                        Fostering enterprises through smart solutions
                    </strong> 
                </h5>
            </div>
            <div class="col-lg-6" style="background-color: #771bf0!important; opacity: .8;">
                <p class="text-white text-justify" style="padding: 33% 10%;">
                    In the current scenario every organization requires smart, connected, secured and data driven solutions to have a remarkable presence and establish competitive advantage to claim dominant market share. We design smart and cost effective solutions for Enterprises through our innovation which nutures a sustainable business model and enhances Operational Efficiency. Using various frameworks, tools & methodologies we excel in creating robust business solutions.
                </p>
            </div>
        </div>
    </div>
</div>
<div class="container-fluid">
    <div class="row fill-main business-body">
        <div class="col-lg-12 mt-5 text-center">
            <img src="images/enterprise-it-Services.jpg" class="img-fluid " alt="">
        </div>
    </div>
</div>
<?php include 'footer.php'; ?>