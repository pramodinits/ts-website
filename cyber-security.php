<?php include 'header.php'; ?>
<?php include 'inner-nav.php'; ?>
<div class="cyber-security-bg">     
    <div class="container">
        <div class="row fill-main features-rightbanner">
            <div class="col-lg-6 col-sm-12 col-xs-12 mt-5 text-center">
                <h3 class="wow fadeInUp text-uppercase" data-wow-delay="400ms" style="font-weight: 900; visibility: visible; animation-delay: 400ms; animation-name: fadeInUp;">Cyber <span class="text-ray">Security</span></h3>
                <div class="text-center">
                    <img class="img-fluid" src="images/gradient.png"/>
                </div>
                <p class="text-left">
                    Cybersecurity or information technology security is a preventive technology dealing with the process of protecting internet-connected  networks, devices, programs, and data from attack, damage, or unauthorized access systems referred to as cyberattacks. As per a 2018 Gartner study, by 2022, the global cybersecurity market is projected to reach US$170.4 billion. Cybersecurity has immense scope as it is inevitable to shield any organization’s security architecture whether it belongs to public or private sector. We at TS aspire to create cyber resilient environment.
                </p>
            </div>
        </div>
    </div>
</div>
<div class="block-responsive">
    <div class="cyber-security-bg-responsive"></div>
    <div class="col-lg-6 col-sm-12 col-xs-12 mt-5 text-center">
        <h3 class="wow fadeInUp text-uppercase" data-wow-delay="400ms" style="font-weight: 900; visibility: visible; animation-delay: 400ms; animation-name: fadeInUp;">Cyber <span class="text-ray">Security</span></h3>
        <div class="text-center">
            <img class="img-fluid" src="images/gradient.png"/>
        </div>
        <p class="text-left">
            Cybersecurity or information technology security is a preventive technology dealing with the process of protecting internet-connected  networks, devices, programs, and data from attack, damage, or unauthorized access systems referred to as cyberattacks. As per a 2018 Gartner study, by 2022, the global cybersecurity market is projected to reach US$170.4 billion. Cybersecurity has immense scope as it is inevitable to shield any organization’s security architecture whether it belongs to public or private sector. We at TS aspire to create cyber resilient environment.
        </p>
    </div>
</div>
<div class="clearfix"></div>
<div class="container">
    <div class="row mt-4">
        <div class="features-header col-md-12">
            <h5><strong>The Thoughtspheres Cybersecurity team is adept at handling all aspects of Cyber security ranging from:</strong></h5>
        </div>
        <div class="aiml-features col-md-12">
            <ul>
                <li>Network security</li>
                <li>Application security</li>
                <li>Cloud security</li>
                <li>Mobile security</li>
                <li>Endpoint security</li>
                <li>Data security</li>
                <li>Identity management</li>
                <li>Database and infrastructure security</li>
                <li>Disaster recovery/business continuity planning</li>
                <li>End-user education</li>
            </ul>
        </div>
    </div>
</div>
<?php include 'footer.php'; ?>