<?php include 'header.php'; ?>
<?php include 'inner-nav.php'; ?>
<div class="clearfix"></div>
<!--top banner--->
<div class="tp-bgimg" id="news">
    <div class="container-fluid">
        <div class="row fill-main">
            <div class="col-lg-12 text-center">
                <h1 class="wow fadeInUp text-uppercase" data-wow-delay="400ms">News</h1>
                <div class="text-center">
                    <img class="img-fluid" src="images/gradient_news.png"/>
                </div>
            </div>
        </div>
    </div>
</div>
<!--top banner--->

<!--news content-->
<div class="container" >
    <section id="news-content">
        <div class="row">
            <div class="col-lg-12 mt-0 text-center">
                <h4 class="text-left mt-4 wow fadeInUp" data-wow-delay="300ms"><b>PATANJALI’s Augmented Reality Mobile App Contract</b> </h4>
                <div class=" wow fadeInUp text-left mt-2">
                    <input type="checkbox" class="read-more-state" id="post-1" />
                    <p class="read-more-wrap">ThoughtSpheres bags PATANJALI’s Augmented Reality Mobile App Contract</p>
                    <p class="read-more-target">
                        On 7th Jan 2017,PATANJALI, India’s leading retail company chose ThoughtSpheres as the technology partner for developing its product based Augmented Reality Mobile App.
                    </p>
                    <label for="post-1" class="read-more-trigger">
                        <!--<i class="fa fa-angle-right"></i>-->
                    </label>
                </div>
                <div class="border border-success text-white"></div>
            </div>                   
        </div>
        <div class="row">
            <div class="col-md-6  border-right">
                <h4 class="text-left mt-4 wow fadeInUp"><b>FbStart Bootstrap Program</b> </h4>
                <div class=" wow fadeInUp text-left mt-2">
                    <input type="checkbox" class="read-more-state" id="post-2" />
                    <p class="read-more-wrap">Selected for FB Start Bootstrap Program</p>
                    <p class="read-more-target">
                        We are pleased to announce that, our ‘Inspect’ App got selected by FbStart for their Bootstrap program.
                    </p>
                    <label for="post-2" class="read-more-trigger"></label>
                </div>
                <div class="border border-success text-white"></div>
                <div class="row">
                    <div class="col-md-12">
                        <h4 class="text-left mt-4 wow fadeInUp"><b>Mobile Applications Conference</b> </h4>
                        <div class=" wow fadeInUp text-left mt-2">
                            <input type="checkbox" class="read-more-state-img" id="post-3" />
                            <p class="read-more-wrap">Thoughtspheres displayed its Augmented Reality (AR) product mSense at Mobile Applications Conference (MAC) held in New Delhi on 4th May 2013.</p>
                            <p class="read-more-target-img">
                            <img src="images/news/news-conference.jpg" alt="" class="img-fluid">
                            </p>
                            <label for="post-3" class="read-more-trigger-img"></label>
                        </div>
                        <div class="border border-success text-white"></div>
                    </div>
                </div>
            </div>
            <div class="col-md-6">

                <h4 class="text-left mt-4 wow fadeInUp"><b>Release of Inspect APP</b> </h4>
                <div class=" wow fadeInUp text-left mt-2">
                    <input type="checkbox" class="read-more-state" id="post-4" />
                    <p class="read-more-wrap">Our first consumer app-Inspect available on Android platform</p>
                    <p class="read-more-target">
                        Thoughtspheres is pleased to announce that, its first consumer Mobile App ‘Inspect’ (version 1.1.1) is now available for Android platform only. Visit http://inspect.co.in to know more details.
                    </p>
                    <label for="post-4" class="read-more-trigger"></label>
                </div>

                <div class="border border-success text-white"></div>

                <div class="row">
                    <div class="col-md-12">
                        <h4 class="text-left mt-4 wow fadeInUp"><b>Article on Augmented Reality</b></h4>
                        <div class=" wow fadeInUp text-left mt-2">
                            <input type="checkbox" class="read-more-state-img" id="post-5" />
                            <p class="read-more-wrap">Pioneered Augmented Reality based software application in Bhubaneswar</p>
                            <p class="read-more-target-img">
                                <img src="images/news/news-augmented.jpg" alt="" class="img-fluid">
                            </p>
                            <label for="post-5" class="read-more-trigger-img"></label>
                        </div>
                        <div class="border border-success text-white"></div>
                    </div>
                </div>

            </div>
        </div>               
    </section>                        
</div>
<!--news content-->

<div class="clearfix"></div>
<?php include 'ourclient.php'; ?> 
<?php include 'footer.php'; ?>