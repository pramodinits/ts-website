<!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
        <title>thoughtspheres</title>    
        <?php include 'header.php'; ?>
    </head>
    <body data-spy="scroll" data-target=".navbar" data-offset="90">
        <?php include 'inner-nav.php'; ?>
        <div style="background-color:#fffff;" >
            <div class="container" >
                <div class="row ">
                    <div class="col-lg-6 mt-2 " style="padding-top:100px;">
                        <h3 class="wow fadeInUp text-black" data-wow-delay="300" style="visibility: visible; animation-name: fadeInUp;">Microsoft Platform </h3>
                        <div class="border border-success text-white"></div>
                        <h5 class="mt-3 wow fadeInUp" data-wow-delay="300ms" style="visibility: visible; animation-delay: 300ms; animation-name: fadeInUp;"><p class="text-justify">Microsoft .Net,the vast software environment and development framework assumes key focus at Thoughtspheres (TS). Our strong technical team in this domain conducts continuous training and support in order to build their capabilities, to impeccably meet client requirement and prospects executing POCs.</p>
                            Capabilities:</h5>
                        <ul class="pl-3 wow fadeInUp" data-wow-delay="600ms" style="visibility: visible; animation-delay: 600ms; animation-name: fadeInUp;">
                            <li> Expertise in VB.Net, ASP.Net, C# and SQL Server etc</li>
                            <li>Experience in .Net Architecture </li>
                            <li> Experience in industry specific products and tools  </li>
                        </ul> 
                    </div>
                    <div class="col-lg-6 mt-5 pt-5 pb-5">
                        <img src="images/tech-mobility.png" class="img-fluid" style="margin-top: 30%;">
                    </div>
                </div>
            </div>
        </div>
        <!--header end-->
        <div class="clearfix"></div>
        <?php include 'ourclient.php'; ?>
        <!-- Footer end -->
        <?php include 'footer.php'; ?>

    </body>


</html>