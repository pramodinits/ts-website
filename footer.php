<div class="" id="">
    <div class="container mt-5 mb-5 pt-2 pb-5">
        <div class="row">
            <div class="col-lg-12 text-center">
                <h2 class="wow fadeInUp text-black font-weight-bold" data-wow-delay="300" style="visibility: visible; animation-name: fadeInUp;">Our Clients</h2>
                <img class="img-fluid" src="images/gradient.png"/>
            </div>
            <!--             <div class="col-md-4 pr-4 wow fadeInRight order-md-2" style="visibility: visible; animation-name: fadeInRight;">
                            <img src="images/ourclient.png" class=" img-fluid pull-right" alt="">
                        </div>-->
            <div class="col-md-12 mt-5 order-md-1">
                <div class="owl-one owl-carousel owl-theme">
                    <div class="item"><img src="images/client/save.png" alt=""> </div>
                    <div class="item"><img src="images/client/logo_orimark.png" alt=""></div>
                    <div class="item"><img src="images/client/navedas.png" alt=""> </div>
                    <div class="item"><img src="images/client/patanjali.png" alt=""></div>
                    <!--<div class="item"><img src="images/client/oneglint-logo.png" alt=""></div>--> 
                    <div class="item"><img src="images/client/pcm.png" alt=""></div>
                    <div class="item"><img src="images/client/oneglint-logo.png" alt=""></div>
                    <div class="item"><img src="images/client/ecociate.png" alt=""></div>
                    <div class="item"><img src="images/client/skycobalt.png" alt=""> </div>
                    <div class="item"><img src="images/client/univate.png" alt=""> </div> 
                </div>
                <div class="owl-two owl-carousel owl-theme mt-5" style="display: none;">
                    <div class="item"><img src="images/client/pcm.png" alt=""></div>
                    <div class="item"><img src="images/client/ecociate.png" alt=""></div>
                    <div class="item"><img src="images/client/skycobalt.png" alt=""> </div>
                    <div class="item"><img src="images/client/univate.png" alt=""> </div>
                    <!--<div class="item"><img src="images/client/oneglint-logo.png" alt=""></div>-->
                    <div class="item"><img src="images/client/save.png" alt=""> </div>
                    <div class="item"><img src="images/client/logo_orimark.png" alt=""></div>
                    <div class="item"><img src="images/client/navedas.png" alt=""> </div>
                    <div class="item"><img src="images/client/patanjali.png" alt=""></div>
                    <div class="item"><img src="images/client/oneglint-logo.png" alt=""></div>
                </div>
            </div>

        </div>
    </div>


    <div class="address">
        <div class="container">
            <div class="row d-flex">
                <div class="col-lg-4 col-md-4 col-sm-6 col-12 wow fadeInUp">
                    <div class="row">
                        <div class="overlay-info">
                            <h4 class="text-uppercase font-weight-light mb-5">Follow Us </h4>
                            <p><i>Explore our brand for TS Synergy</i></p>
                            <a href="http://writobox.com/">
                                <img src="images/writobox.png"  class="img-fluid">
                            </a>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-12">
                            <div class="footer_social_icons mt-5">
                                <ul class="mb-0 p-0 text-left">
                                    <li><a href="https://www.facebook.com/thoughtsSpheres/" class="facebook_bg_hvr text-white border"><i class="fa fa-facebook"></i></a> </li>
                                    <li><a href="https://www.linkedin.com/company/thoughtspheres-technologies-pvt-ltd-/" class="twitter_bg_hvr text-white border"><i class="fa fa-linkedin" aria-hidden="true"></i></a> </li>
                                    <li><a href="#." class="twitter_bg_hvr text-white border"><i class="fa fa-twitter"></i></a> </li>
                                </ul>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-lg-4 col-md-4 col-sm-6 col-12">
                    <div class="address-box wow fadeInUp " data-wow-delay="300">
                        <h4 class="text-uppercase font-weight-light mb-5">Get In Touch</h4>
                        <h5 class="font-weight-bold">ThoughtSpheres Technologies Pvt . Ltd.</h5>
                        <p>
                            First Floor, Sai Saraswati Complex, 81, District Centre <br>
                            Niladri Vihar, Chandrashekharpur, Bhubaneswar
                        </p>
                        <br/>
                        <div class="row">
                            <div class="col-12 pr-0">
                                <a class="text-black" href="mailto:contactus@thoughtspheres.com"><span class="text-right"><i class="fa fa-envelope"></i> Mail:contactus@thoughtspheres.com</span>             
                                </a> 
                            </div>
                        </div>
                        <div class="clearfix"></div>
                    </div>
                </div>
                <div class="col-lg-4 col-md-4 col-sm-6 col-12 site-links">
                    <h4 class="text-uppercase font-weight-light mb-5">Links</h4>
                    <div class="btm-footerm" style="display: flex;">
                        <div class="col-6">
                            <ul>
                                <li><a href="index.php">Home</a></li>
                                <li><a href="about.php">About Us</a></li>
                                <li><a href="ai-ml.php"> Technologies</a></li>
                            </ul>
                        </div>
                        <div class="col-6">
                            <ul>
                                <li><a href="news.php">Resources</a></li>
                                <li><a href="career.php"> Careers</a></li>
                                <li><a href="contactus.php"> Contact Us</a></li>
                            </ul>
                        </div>
                    </div>  
                </div>
            </div>
            <div class="row">
                <div class="col-12">
                    <div class="mt-4 text-center">
                            <small>Copyright <?php echo date('Y'); ?> 
                                <b>ThoughtSpheres Technologies Pvt. Ltd.</b> 
                                <a href="privacy-policy.php">All rights reserved. Privacy Policy</a>
                            </small>
                    </div>
                </div>
            </div>
        </div>

        <div class="clearfix"></div>
        <script>
            $(document).ready(function () {
                $('.owl-one').owlCarousel({
                    loop: true,
                    margin: 10,
                    autoplay: true,
                    nav: true,
                    rtl: true,
                    responsive: {
                        0: {
                            items: 1
                        },
                        600: {
                            items: 4
                        },
                        1000: {
                            items: 5
                        }
                    }
                });

                $('.owl-two').owlCarousel({
                    loop: true,
                    margin: 10,
                    nav: true,
                    autoplay: true,
                    rtl: false,
                    responsive: {
                        0: {
                            items: 1
                        },
                        600: {
                            items: 4
                        },
                        1000: {
                            items: 5
                        }
                    }
                });
            });
        </script>
        <div class="clearfix"></div>
    </div>
</div>
<script src="js/v53/paricles-3d.js"></script>
<script src="js/typeit.min.js"></script> 
<script src="js/swiper.min.js"></script>
<script>
// When the user scrolls down 20px from the top of the document, slide down the navbar
        window.onscroll = function () {
            scrollFunction();
        };
        function scrollFunction() {
            if (document.body.scrollTop > 40 || document.documentElement.scrollTop > 40) {
                // hide the horizontal menu action bar
                document.getElementById("navbar").style.top = "0";
            } else {
                // show the horizontal menu action bar
                document.getElementById("navbar").style.top = "-100px";
                // hide the left menu panel
                $('#btn_sideNavClose').click();
            }
        }
</script>

<script src="js/script.js"></script>
<script src="js/popper.min.js"></script>
<script src="js/bootstrap.min.js"></script>
<script src="js/jquery.appear.js"></script>
<!-- parallax Background -->
<script src="js/parallaxie.js"></script>
<script src="js/jquery.cubeportfolio.min.js"></script>
<!-- owl carousel slider -->
<script src="js/owl.carousel.js"></script>
<script src="js/wow.min.js"></script>
<!-- REVOLUTION JS FILES -->
<script src="js/jquery.themepunch.tools.min.js"></script>
<script src="js/jquery.themepunch.revolution.min.js"></script>
<script src="js/revolution.extension.actions.min.js"></script>
<script src="js/main.js"></script>
</body>
</html>

