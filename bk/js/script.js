var swiper = new Swiper('.main-slider-header', {
    parallax: true,
});

$('#type_it_slider').typeIt({
    speed: 100,
    autoStart: false,
    loop: true,
})
    .tiType('<strong style="font-weight: 300, font-size:8px;">Get digitally </strong> decked up with US- TS...ThoughtSpheres')
    .tiSettings({speed: 700})
    .tiPause(1000)
    .tiSettings({speed: 50})
    .tiDelete()
     .tiType('<strong style="font-weight: 300, font-size:8px;">Get digitally </strong> decked up with US- TS...ThoughtSpheres')
    .tiSettings({speed: 700})
    .tiPause(1000)
    .tiSettings({speed: 50})
    .tiDelete()
    .tiType('<strong style="font-weight: 300, font-size:8px;">Get digitally </strong> decked up with US- TS...ThoughtSpheres')


$(".scroll").on('click', function(event) {
    event.preventDefault();
    $('html,body').animate({ scrollTop: $(this.hash).offset().top }, 1000);
});

//Wow Initializing
new WOW().init();

window.onload = function() {
    Particles.init({
        selector: '.background_3',
        color: '#ffffff',
        connectParticles: true,
        sizeVariations: 9,
        maxParticles: 200,
    });
};