<!DOCTYPE html>
<html lang="en">
    <?php include 'header.php'; ?>
    <body data-spy="scroll" data-target=".navbar" data-offset="90">
        <?php include 'inner-nav.php'; ?>
        <section class="contact-form circle-bg2 wow fadeInUp" data-wow-delay="300" style="visibility: visible; animation-name: fadeInUp;">
            <div class="">
                <div class="row">
                    <div class="col-md-6 indigo">
                        <div style="padding:30px 140px;">
                        <div class="m-0 wow fadeInUp " data-wow-delay="300" style="visibility: visible; animation-name: fadeInUp;">
                            <h2 class="text-capitalize text-white">Address </h2>
                            <h4 class="text-success mt-5">India Office</h4>
                            <p class="pb-4 text-white">HIG-45, First Floor, Jaydev Vihar, Bhubaneswar</p>
                            <h4 class="text-success mt-5">UK Office</h4>
                            <p class="pb-4 text-white">15 Tamer Road, Sleaford Engand NG34 7GS </p>
                            <h2 class="text-capitalize text-white">General Support</h2>
                            <p class="pb-4 text-white">contactus@thoughtsphres.com</p> 
                        </div>  
                        </div>
                    </div>
                    <div class="col-md-6" style="padding:0 80px;">
                        <form action="#." class="mt-50">
                            <div class="row">
                                <div class="col-md-12">
                                    <div class="row no-gutters">
                                        <div class="col-12">
                                            <h4 class="text-center">SEND US MESSAGE</h4>
                                            <div class="inputbox">
                                                Tele Your Name
                                            </div>
                                        </div>
                                        <div class="clearfix"></div>
                                        <div class="col-6 col-6 no-gutters"> 
                                            <input type="text" class="inputbox" id="lname" placeholder="First Name" required="">
                                        </div>
                                        <div class="col-6 col-6 ">
                                            <input type="email" class="inputbox" id="email" placeholder="Last Name" required="">
                                        </div>
                                    </div>
                                    <div class="row no-gutters mt-1">
                                        <div class="col-12">
                                            <div class="inputbox">
                                                Enter Your E-mail *
                                            </div>
                                        </div>
                                        <div class="clearfix"></div>
                                        <div class="col-12 col-12 no-gutters">
                                            <input type="text" class="inputbox" id="lname" placeholder="Enter Yur E-mail" required="">
                                        </div>
                                    </div>
                                    <div class="row no-gutters mt-1">
                                        <div class="col-12">
                                            <div class="inputbox">
                                                Enter Your Phone No
                                            </div>
                                        </div>
                                        <div class="clearfix"></div>
                                        <div class="col-12 col-12 no-gutters"> 
                                            <input type="text" class="inputbox" id="lname" placeholder="Enter Phone No" required="">
                                        </div>
                                    </div>
                                    <div class="row no-gutters mt-1">
                                        <div class="col-12">
                                            <div class="inputbox">
                                                Message
                                            </div>
                                        </div>
                                        <div class="clearfix"></div>
                                        <div class="col-12 col-12 no-gutters">                                  
                                            <textarea rows="2" id="message" placeholder="Message" required="" style="width: 100%" class="p-4"></textarea>                              
                                        </div>
                                    </div>
                                </div>
                                <div class="col-md-6">
                                </div>
                            </div>
                            <div class="row mt-1 checkbox-row">
                                <div class="col-md-12 center-block">
                                    <button type="submit" class="btn custom-btn1 hvr-shutter-out-horizontal-blk text-center ">Submit Request</button>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </section>
        <?php include 'footer.php'; ?>
    </body>
</html>