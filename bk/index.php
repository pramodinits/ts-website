<style>
   .slick-slide{
  img{
    width: 100%;
  }
}
</style>
<!DOCTYPE html>
<html lang="en">
    <?php include 'header.php'; ?>
    <body data-spy="scroll" data-target=".navbar" data-offset="90">
        <?php include 'nav.php'; ?>      
        <div class="marshall-container" >    
            <img src="images/bg_main.jpg" class=" pull-right" alt="">
            <a href="#." title="Logo" class="logo scroll">
                <img src="images/logo.png" alt="logo" class="logo-light default img-fluid" style="position:absolute; top:250px; right:147px; "> </a>

            <div class="row">               
                <div class="marshall-col-12 marshall-col-content align-center ">
                    <div class="">
                        <div id="triangle_canvas" class="mrs_canvas mrs_particles_canvas">
                            <canvas id="particle_3d" width="900" height="520" ></canvas>
                        </div>
                    </div>
                </div>
            </div>
            <div class="container">
                <h3 id="type_it_slider" class="raleway swiper-slider-heading" style="position: absolute; left:20%; right: 0; z-index: 9; margin-top:-10%; color:white; font-weight:normal;"><i class="ti-placeholder" style="display:inline-block;width:0;line-height:0;overflow:hidden;">.</i><span style="display:inline;position:relative;font:inherit;color:inherit;" class="ti-container">Best Suitable for <strong style="font-weight: 600">Creative Agenc</strong></span><span style="display: inline; position: relative; font: inherit; color: inherit; opacity: 0.0649081;" class="ti-cursor">|</span></h3>
            </div>
        </div>

        <section class="" id="about">
            <div class="container">
                <div class="row">
                    <div class="col-lg-4 pr-5 wow fadeInLeft">
                        <div class="img-hvr "></div>
                        <img src="images/aboutus.png" class=" pull-right" alt="">                        
                    </div>
                    <div class="col-lg-8 pl-6">
                        <h2 class="wow fadeInUp" data-wow-delay="400ms">We create smart tech-savvy environment</h2>
                        <p class="pt-3 wow fadeInUp" data-wow-delay="500ms">We are a leading edge IT and consulting firm possessing a strong leadership team obsessed with a technology-driven approach loaded with rich global corporate experience. We aspire to be partners in overcoming the emerging technical challenges faced by the Enterprise Community and IT Users globally. We deal with Actuate Technology & Consulting Services by leveraging our unparalleled expertise and analytical insights. Our sphere of influence predominantly lies in the IT Landscape as we design smart, innovative products as well simple yet exceedingly effective services for enriched business transformation.</p>
                    </div>
                </div>
            </div>
        </section>
        <section class="" id="about">
            <div class="container">
                <div class="row">
                    <div class="col-lg-8 pl-6">
                        <h2 class="wow fadeInUp" data-wow-delay="400ms">Develop, Disrupt, 
                            Disperse with Digital </h2>
                        <p class="pt-3 wow fadeInUp" data-wow-delay="500ms">TS Digital Services is comprised of TS Synergy,TS Mobility and TS Analytics which aim at extending seamless consumers & enterprise experience. TS Synergy provides a comprehensive marketing solution ranging from creative design to digital campaigns utilizing our expertise in the field of content and social media to e-learning packages for enhanced customer engagement and consequently business generation. TS Mobility claims exquisite authority,having created Innovative Mobility solutions for numerous clients.We have expertise in the niche technology called AR . TS Analytics team possesses superior dexterity in helping the enterprise enjoy leading market position through critical data insights and analysis .</p>
                    </div>
                    <div class="col-lg-4 pr-4 wow fadeInRight">
                        <div class="img-hvr "></div>
                        <img src="images/businessunits.png" class=" pull-right" alt="">
                    </div>
                </div>
            </div>
        </section>
        <section class="" id="about">
            <div class="container">
                <div class="row">
                    <div class="col-lg-4 wow fadeInLeft" style="visibility: visible; animation-name: fadeInLeft;">
                        <div class="img-hvr"></div>
                        <img src="images/technology_capabilities.png" class="  " alt="">
                    </div>
                    <div class="col-lg-8 wow fadeInUp" style="visibility: visible; animation-name: fadeInRight;">
                        <div class="row">
                            <div class="col-md-3 col-6 ">
                                <div class="image-box">            
                                    <img src="images/ar.png" class="img-fluid">
                                    <div class="techno-text center-block">
                                        <a href="technology.php" class="text-black "><img src="images/mobility_1.png" class="img-fluid"></a>
                                        <div class="text-success">Mobility</div>
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-3 col-6">
                                <div class="image-box">
                                    <img src="images/ar.png" class="img-fluid">
                                    <div class="techno-text center-block">
                                        <a href="augmented.php" class="text-black "> <img src="images/ar_1.png" class="img-fluid"></a>
                                        <div class="text-success text-center">Argumented Reality</div>
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-3 col-6">
                                <div class="image-box">
                                    <img src="images/ar.png" class="img-fluid">
                                    <div class="techno-text center-block">
                                        <a href="iot.php" class="text-black "> <img src="images/iot_1.png" class="img-fluid"></a>
                                        <div class="text-success text-center">IOT</div>
                                    </div>
                                </div>
                            </div>

                            <div class="col-md-3 col-6">
                                <div class="image-box">
                                    <img src="images/ar.png" class="img-fluid">
                                    <div class="techno-text center-block">
                                        <a href="analytics.php" class="text-black "><img src="images/analytics_1.png" class="img-fluid"></a>
                                        <div class="text-success text-center">Analytics</div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="row mt-3">
                            <div class="col-md-3 col-6">
                                <div class="image-box">
                                    <img src="images/ar.png" class="img-fluid">
                                    <div class="techno-text center-block">
                                        <a href="webplatform.php" class="text-black "><img src="images/web-platform_1.png" class="img-fluid"></a>
                                        </a>
                                        <div class="text-success text-center">Web Platform</div>
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-3 col-6">
                                <div class="image-box">
                                    <img src="images/ar.png" class="img-fluid">
                                    <div class="techno-text center-block">
                                        <a href="databaseplatform.php" class="text-black "><img src="images/database_1.png" class="img-fluid"></a>
                                        <div class="text-success text-center">Database Platform</div>
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-3 col-6">
                                <div class="image-box">
                                    <img src="images/ar.png" class="img-fluid">
                                    <div class="techno-text center-block">
                                        <a href="wbiplatform.php" class="text-black "><img src="images/dwbi_1.png" class="img-fluid"></a>
                                        <div class="text-success text-center">DW/BI Platform</div>
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-3 col-6">
                                <div class="image-box">
                                    <img src="images/ar.png" class="img-fluid">
                                    <div class="techno-text center-block">
                                        <a href="smartuiplatform.php" class="text-black "><img src="images/smart-ui_1.png" class="img-fluid"></a>
                                        <div class="text-success text-center">Smart UI Platform</div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </section>
        <section class="counter-bg pb-0">
            <div class="container">
                <div class="row">
                    <div class="col-lg-6">
                         <div class="owl-one owl-carousel owl-theme">
    <div class="item" style=" "> <img src="images/logo_orimark.png" alt=""></div>
    <div class="item" style=" "> <img src="images/navedas.png" alt=""> </div>
    <div class="item" style=""> <img src="images/oneglint-logo.png" alt=""></div>
</div>
 <hr>

<div class="owl-two owl-carousel owl-theme">
    <div class="item" style=""><img src="images/patanjali.png" alt=""></div>
    <div class="item" style=""><img src="images/pcm.png" alt=""></div>
    <div class="item" style=""><img src="images/save.png" alt=""> </div>
    
</div>
                    </div>
                    <div class="col-lg-6 pr-4 wow fadeInRight" style="visibility: visible; animation-name: fadeInRight;">

                        <img src="images/ourclient.png" class=" pull-right" alt="">
                    </div>
                </div>

            </div>
        </section>
        <script>
        $(document).ready(function(){
    $('.owl-one').owlCarousel({
        loop:true,
        margin:10,
        autoplay:true,
        nav:true,
        rtl: true,
        
        responsive:{
            0:{
                items:1
            },
            600:{
                items:2
            },
            1000:{
                items:3
            }
        }
    });

    $('.owl-two').owlCarousel({
        loop:true,
        margin:10,
        nav:true,
        autoplay:true,
        rtl: false,
        responsive:{
            0:{
                items:1
            },
            600:{
                items:2
            },
            1000:{
                items:3
            }
        }
    });
    
});
        </script>

        <?php include 'footer.php'; ?>
    </body>
</html>