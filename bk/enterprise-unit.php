<!DOCTYPE html>
<html lang="en">
    <head>
        
      
      
        <!-- Modernizr js - required -->
      
        
        <!-- Required meta tags -->
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
        <title>thoughtspheres</title>    
        <?php include 'header.php';?>
    </head>
    <body data-spy="scroll" data-target=".navbar" data-offset="90">
        <!--loader start-->
        <!--loader end-->

        <!--header start-->

         <?php include 'inner-nav.php';?>
        <div style="background-color:#fffff;" >
            <div class="container" >
                <div class="row ">
                    <div class="col-lg-6 mt-2 " style="padding-top:128px;">
                        <h3 class="wow fadeInUp text-black" data-wow-delay="300" style="visibility: visible; animation-name: fadeInUp;">ENTERPRISE IT </h3>
                        <div class="border border-success text-white"></div>
                        <h5 class="mt-3 wow fadeInUp" data-wow-delay="300ms" style="visibility: visible; animation-delay: 300ms; animation-name: fadeInUp;">Fostering enterprises through smart solutions</h5>
                        <div class="wow fadeInUp" data-wow-delay="600ms" style="visibility: visible; animation-delay: 600ms; animation-name: fadeInUp;">
                    In the current scenario every organization requires smart, connected, secured and data driven solutions to have a remarkable presence and establish competitive advantage to claim dominant market share. We design smart and cost effective solutions for Enterprises through our innovation which nutures a sustainable business model and enhances Operational Efficiency. Using various frameworks, tools & methodologies we excel in creating robust business solutions.
                </div>
                    </div>
                    <div class="col-lg-6 mt-5 pt-5 pb-5">
                        <img src="images/tech-mobility.png" class="img-fluid">
                    </div>
                </div>
            </div>
        </div>
        <!--header end-->
        <div class="clearfix"></div>
       <section class="counter-bg pb-0">
            <div class="container">
                <div class="row">
                    <div class="col-lg-12">
                       
                        <div class="wow fadeInUp" data-wow-delay="400ms" style="visibility: visible; animation-delay: 400ms; animation-name: fadeInUp;">
                              <img src="images/Enterprise-IT.jpg" class="img-fluid">
                    </div>
                    <div class="wow fadeInRight" style="visibility: visible; animation-name: fadeInRight;">
                        <div class="img-hvr "></div>
                        <img src="images/ourclient.png" class=" pull-right" alt="">
                    </div>
                </div>
            </div>
        </section>
      <div class="container">
                <div class="brand-carousel bottom50 owl-carousel owl-theme owl-loaded owl-drag">
                    <div class="owl-stage-outer">
                        <div class="owl-stage" style="transform: translate3d(-1596px, 0px, 0px); transition: all 0.25s ease 0s; width: 4560px;">
                            <div class="owl-item " style="width: 198px; margin-right: 30px;">
                                <div class="item"> <img src="images/logo_orimark.png" alt=""> </div>
                            </div>
                            <div class="owl-item " style="width: 198px; margin-right: 30px;">
                                <div class="item"> <img src="images/navedas.png" alt=""> </div>
                            </div>
                            <div class="owl-item " style="width: 198px; margin-right: 30px;">
                                <div class="item"> <img src="images/oneglint-logo.png" alt=""> </div>
                            </div>
                            <div class="owl-item " style="width: 198px; margin-right: 30px;">
                                <div class="item"> <img src="images/patanjali.png" alt=""> </div>
                            </div>
                            <div class="owl-item " style="width: 198px; margin-right: 30px;">
                                <div class="item"> <img src="images/pcm.png" alt=""> </div>
                            </div>
                            <div class="owl-item" style="width: 198px; margin-right: 30px;">
                                <div class="item"> <img src="images/save.png" alt=""> </div>
                            </div>
                        </div>
                    </div>
                    <div class="owl-nav disabled">
                        <button type="button" role="presentation" class="owl-prev"><span aria-label="Previous">‹</span></button>
                        <button type="button" role="presentation" class="owl-next"><span aria-label="Next">›</span></button>
                    </div>
                    <div class="owl-dots disabled"></div>
                </div>
            </div>
         

            <!-- address Start -->
          
            <!-- Footer end -->
           <?php include 'footer.php';?>
<!-- Footer end -->
<script src="js/v53/paricles-3d.js"></script>
<!-- Optional JavaScript -->
<script src="js/jquery-3.1.1.min.js"></script>
<script src="js/typeit.min.js"></script>       
<!-- Particles Core JavaScript -->
<!-- Swiper JavaScript -->
<script src="js/swiper.min.js"></script>
<!-- Portfolio JavaScript -->
<!-- Custom JavaScript -->
<script src="js/script.js"></script>

<script>
// When the user scrolls down 20px from the top of the document, slide down the navbar
window.onscroll = function() {scrollFunction();};
function scrollFunction() {
  if (document.body.scrollTop > 40 || document.documentElement.scrollTop > 40) {
       // hide the horizontal menu action bar
    document.getElementById("navbar").style.top = "0";    
  } else {
      // show the horizontal menu action bar
    document.getElementById("navbar").style.top = "-100px";
    // hide the left menu panel
     $('#btn_sideNavClose').click();    
  }
}
</script>
<!--<script src="http://www.themesindustry.com/html/weone-2019/js/jquery-3.3.1.min.js"></script>-->
<script src="js/popper.min.js"></script>
<script src="js/bootstrap.min.js"></script>
<script src="js/jquery.appear.js"></script>
<!-- parallax Background -->
<script src="js/parallaxie.js"></script>
<!-- fancybox popup  missing-->
<!--<script src="js/jquery.fancybox.min.js"></script> -->
<!-- isotop gallery missing -->
<!--<script src="js/isotope.pkgd.min.js"></script>-->
<!-- cube portfolio gallery -->
<script src="js/jquery.cubeportfolio.min.js"></script>
<!-- owl carousel slider -->
<script src="js/owl.carousel.js"></script>
<!-- typewriter effect on slider missing -->
<!--<script src="js/typewriter.js"></script>-->
<!--CountTo JS-->
<!--<script src="js/jquery-countTo.js"></script>-->
<!--Wow JS missing-->
<script src="js/wow.min.js"></script>
<!-- REVOLUTION JS FILES -->
<script src="js/jquery.themepunch.tools.min.js"></script>
<script src="js/jquery.themepunch.revolution.min.js"></script>
<!-- SLIDER REVOLUTION 5.0 EXTENSIONS  (Load Extensions only on Local File Systems !  The following part can be removed on Server for On Demand Loading) -->
<script src="js/revolution.extension.actions.min.js"></script>
<!--<script src="js/revolution.extension.carousel.min.js"></script>-->
<script src="js/revolution.extension.kenburn.min.js"></script>
<script src="js/revolution.extension.layeranimation.min.js"></script>
<script src="js/revolution.extension.migration.min.js"></script>
<script src="js/revolution.extension.navigation.min.js"></script>
<script src="js/revolution.extension.parallax.min.js"></script>
<script src="js/revolution.extension.slideanims.min.js"></script>
<!-- map -->
<script src="js/map.js"></script>
<!-- custom script -->
<script src="js/main.js"></script>
    </body>


</html>