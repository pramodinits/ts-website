<section class="address bg-white pb-0" id="contact">
    <div class="">
        <div class="row d-flex m-0 p-0">
            <div class="col-lg-7 order-lg-1 ">
                <div class="address-box wow fadeInUp " data-wow-delay="300">
                    <h2 class="text-capitalize mb-2">Get In Touch</h2>
                    <h4 class="mb-0">ThoughtSpheres Technologies Pvt . Ltd.</h4>
                    <p>HIG- 45, First Floor, Jaydev Vihar, Bhubaneswar 751013, India</p>
                    <div class="row">
                        <div class="col-6 pr-0">
                            <a class="text-black mt-2" href="mailto:contactus@thoughtspheres.com"><i class="fa fa-envelope-o pr-2"></i><span class="text-right"> Mail:contactus@thoughtspheres.com</span> |              
                            </a> 
                        </div>
                        <div class="col-6 p-0">
                            <div class="footer_social_icons mt-2">
                                <ul class="mb-0">
                                    <li><a href="#." class="facebook_bg_hvr text-white"><i class="fa fa-facebook"></i></a> </li>
                                    <li><a href="#." class="twitter_bg_hvr text-white"><i class="fa fa-twitter"></i></a> </li>
                                    <li><a href="#." class="googleplus_bg_hvr text-white"><i class="fa fa-google-plus"></i></a> </li>
                                </ul>
                            </div>
                        </div>
                    </div>
                    <div class="btm-footer">
                        <ul class="mb-0">
                            <li><a href="index.php">Home</a></li>
                            <li>|</li>
                            <li><a href="aboutus.php">About Us</a></li>
                            <li>|</li>
                            <li><a href="business-unit.php">Business Units</a></li>
                            <li>|</li>
                            <li><a href="technology.php"> Technologies</a></li>
                            <li>|</li>
                            <li><a href="resources.php">Resources</a></li>
                            <li>|</li>
                            <li><a href="career.php"> Careers</a></li>
                            <li>|</li>
                            <li><a href="contact.php"> Contact Us</a></li>
                        </ul>
                        <div>
                            <span><small>Copyright 2019 <b>ThoughtSpheres Technologies Pvt. Ltd.</b> <a href="privacy-policy.php">All rights reserved. Privacy Policy</a></small></span></div>
                    </div>
                </div>
                <div class="clearfix"></div>
            </div>
            <div class="col-lg-5 order-lg-0 p-0 wow fadeInUp">
                <!--Location Map here-->
                        <!--   <img src="images/footer.png"  class="img-fluid" >-->
                <div class="">
                    <img src="images/footer.png"  class="img-fluid" >
                    <div class="centered">
                        <div class="overlay-info">
                            <span class="mb-3"></span>
                            <h2 class="mb-1 text-white">Follow Us </h2>
                            <p>See Our Amazing Work</p>
                            <img src="images/writobox.png"  class="img-fluid" >
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
<script src="js/v53/paricles-3d.js"></script>
<script src="js/typeit.min.js"></script> 
<script src="js/swiper.min.js"></script>
<script>
// When the user scrolls down 20px from the top of the document, slide down the navbar
window.onscroll = function() {scrollFunction();};
function scrollFunction() {
  if (document.body.scrollTop > 40 || document.documentElement.scrollTop > 40) {
       // hide the horizontal menu action bar
    document.getElementById("navbar").style.top = "0";    
  } else {
      // show the horizontal menu action bar
    document.getElementById("navbar").style.top = "-100px";
    // hide the left menu panel
     $('#btn_sideNavClose').click();    
  }
}
</script>
<script src="js/script.js"></script>
<script src="js/popper.min.js"></script>
<script src="js/bootstrap.min.js"></script>
<script src="js/jquery.appear.js"></script>
<!-- parallax Background -->
<script src="js/parallaxie.js"></script>
<script src="js/jquery.cubeportfolio.min.js"></script>
<!-- owl carousel slider -->
<script src="js/owl.carousel.js"></script>
<script src="js/wow.min.js"></script>
<!-- REVOLUTION JS FILES -->
<script src="js/jquery.themepunch.tools.min.js"></script>
<script src="js/jquery.themepunch.revolution.min.js"></script>
<script src="js/revolution.extension.actions.min.js"></script>
<script src="js/main.js"></script>
