<!DOCTYPE html>
<html lang="en">
    <head>
        <!-- Modernizr js - required -->
        <!-- Required meta tags -->
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
        <title>thoughtspheres</title>    
        <?php include 'header.php';?>
    </head>
  
    <body data-spy="scroll" data-target=".navbar" data-offset="90">
        <!--loader start-->
        <!--loader end-->

        <!--header start-->

         <?php include 'inner-nav.php';?>
       
        <div style="background-color:#fffff;" >
            <div class="container" >
                  <section class="about" id="digital" >
               <div class="row">
                   <div class="col-lg-12 mt-5 text-center">
                            <h2 class="wow fadeInUp text-black font-weight-bold" data-wow-delay="300" style="visibility: visible; animation-name: fadeInUp;">BLOGS</h2>
                            <div class="border-success text-white" style="border:solid 1px #28a745;"></div>
                            <h4 class="text-left mt-4 wow fadeInUp" data-wow-delay="300ms" style="visibility: visible; animation-delay: 300ms; animation-name: fadeInUp;"><b>PATANJALI’s Augmented Reality Mobile App Contract</b> </h4>
                            <div class=" wow fadeInUp text-left mt-4" data-wow-delay="600ms" style="visibility: visible; animation-delay: 600ms; animation-name: fadeInUp;">
                                <div class="comment more">
                               The new associate of Artificial Intelligence (AI) appearing in tech-news headlines is Robotic Process Automation (RPA).We all would agree to the fact that AI takes away the limelight in every technology related discussion, so it may be safely assumed that the humanity at large has assimilated the basic connotation of the term.

So now it’s turn to understand what RPA is all about especially as the new boy on the tech-street has already garnered much deserved attention amongst the head honchos of leading companies seeking digitization.
Is RPA all about robots?
<br><br>
Robotic Process Automation (RPA) is one of the most popular Artificial Intelligence applications, currently. It surely has nothing to do with typical shiny physical robots, rather it is a user-friendly generic software tool designed using screen scraping and other technologies to create specialized agents/bots developed to automate highly-structured, repetitive yet dreary tasks thus relieving the knowledge workers. The catch lies in the fact that it allows enterprises with legacy systems (which includes most large non-tech companies) to mechanize their clerical workflows like transferring data from multiple input sources like email and excels to ERP and CRM systems and much more. In spite of the latest buzz around the disruptive technologies like AI and cognitive solutions, RPA while being at an adolescent stage, has still managed to attract many enterprises to adopt new approach towards managing their Business Processes.
Modus Operandi of RPA<br><br>
RPA is mainly intended to automate the repetitive tasks to save the time & cost. In a traditional workflow automation system, there are quite a few options which can be put to use to automate some of the tasks. For instance, Cron Jobs can be implemented in any programming language or scripting language with a list of actions which can run and perform certain tasks at a specific time. On the contrary, RPA software develops the sequence of actions to automate the process by observing the user who performs certain actions using the application’s GUI. Basically, RPA is based on rules which can guide a program to how to respond under certain conditions.
Growth Prediction:<br><br>
According to a report published by MarketsandMarkets called Robotic Process Automation Market by Process (Automated solution, Decision Support & Management Solution, and Interaction Solution), Operation (Rule Based and Knowledge Based), Type (Tools and Services), Industry, and Geography – Global Forecast to 20221, the robotic process automation market is estimated to reach USD 2,467.0 million by 2022, at a CAGR (compound annual growth rate) of 30.14% between 2017 and 2022.
Does it deserve the merit? <br><br>
While automating a process, RPA manages to positively impact various aspects. It achieves speed, accuracy, reliability, scalability, productivity, consistency, simplicity and efficiency. In addition to this, RPA provides satisfactory outcome and ROI for all the involved stakeholders. Businesses using RPA experience cost saving and reduced risk leading to improved business results. Of course, the latest technology deserves to be referred to as simply outstanding.<br><br>
<div class="text-center"><img src="images/barsha.png" class="img-fluid" style=""></div>
Impact on employment:
Though there are mixed views around the globe by different media houses and industry experts regarding the loss of jobs due to RPA but most of positive predictions are now becoming less believable and the truth is, RPA implementation will surely lead to loss of jobs for those who work in automatable tasks requiring minimum skills. RPA is a big threat to BPO industries and it has already started changing the way BPOs work. It will not be wrong if we say this is the only industry which can definitely take strategic advantages offered by RPA most significantly. Soon, traditional BPOs will turn into Robotic BPOs. It is clear that, automation will turn some positions redundant and therefore up-skilling of employees and engaging them in different functions would be a sensible option to prevent large scale unemployment which accompanies obvious negative social as well as individual impact.

According to a joint report conducted by Nasscom and Everest Group2 named “Seizing the Robotic Process Automation market Opportunity”, RPA will change the service provider/BMP(Business Process Management) delivery model which will be tilting more towards a middle heavy delivery model. Therefore, in a positive light, RPA may be considered as a catalyst to growth of high value jobs for skilled engineers and process designers.
<div class="text-center"><img src="images/whether.png" class="img-fluid" style=""></div>
Summing it up:
While this promising technology seems to be branching out from the family tree of AI, yet AI and RPA may also be treated as parallel technologies from some perspectives. What logically follows is, both could be used in synergy to create optimum traction in the world of digitization.
                                </div>

                            </div>
                              <div class="border border-success text-white"></div>
                        </div>
                   
                </div>
                      <div class="row">
                      <div class="col-md-6  border-right">
                            <h4 class="text-left mt-4 wow fadeInUp" data-wow-delay="300ms" style="visibility: visible; animation-delay: 300ms; animation-name: fadeInUp;"><b>Augmented Reality, New or Old?</b> </h4>
                            <div class=" wow fadeInUp text-left mt-4" data-wow-delay="600ms" style="visibility: visible; animation-delay: 600ms; animation-name: fadeInUp;">
                              <div class="comment more">
                             In this world obsessed with ‘give me more’ attitude, even stark reality has earned the status of being lackluster. Augmented reality seems to be the answer by the digital world to this ever excitement-hungry milieu.

As the term literally suggests AR augments, enhances ,laces up the real world experience with additional digitally created features for a whole new and exhilarating experience beyond the real. Therefore, Augmented Reality (AR) is a technology which enriches a user’s view of the real world by super imposing computer-generated data such as sound, video, 3D models and other graphics and GPS overlays etc. thus elevating the senses of the user. It turns the viewing of a mundane object into an audio-visual extravaganza. Similarly, a normal view of a location can also be adorned with loads of relevant/contextual information keeping in view the needs of the user by leveraging this technique.
Recently,the niche technology called Augmented Reality (AR) has garnered the limelight with the popularity of game called Pokémon Go developed by  Niantic for iOS and Android devices where the smart player is allowed to transcend between the real world and the virtual world of Pokémon. This has also been recently endorsed by the corporate biggies like Mark Zuckerberg, Tim Cook to name a few. It has been increasingly believed that the future of technology lies in AR. The AR flight is all set to take off universally.
However, the genesis of AR dates back to 1968. Harvard was the breeding ground of the first AR technology that year when computer scientist Ivan Sutherland (referred to as the “father of computer graphics”) created an AR head-mounted display system. It took more than a couple of decades for the first successful commercial AR application to find its form. It was in the year 2008, that the Munich (Germany) based advertising agency developed AR based advertising campaign for the automobile major BMW. A printed magazine ad of a model BMW Mini, when displayed in front of a computer’s camera, populated a virtual model on the screen. The interested customers were able to control the car on the screen and move it around to view various angles of the car by manipulating the printed ad thus allowing interaction with a digital model in real time. Obviously, it was a ground breaking, exciting experience for the users!
Ever since, the trend has caught up. Various leading brands adopted AR which is also sometimes referred to as Altered Reality to promote their brands. Though the list is endless, a few worth mentioning are Disney(Entertainment) , National Geographic(AV Media), Coca-Cola(Beverage),IKEA(Furniture),Virgin Atlantic (Airlines) ,Bennett & Coleman(Print Media),DeBeers(Jewelry),IBM(Retail),Converse(Shoes),Copenhagen,Canberra Airport(Airport),Axis (Bank),The small list itself bears the testimony to the fact that the cutting edge technology can find its take in almost all industries/sectors. Whether we consider this technology to be new or old, it is here to not only stay rather is surely going to augment!
                                                        
                              </div>

                            </div>
                              <div class="border border-success text-white"></div>
                              <div class="row">
                                <div class="col-md-12 ">
                            <h4 class="text-left mt-4 wow fadeInUp" data-wow-delay="300ms" style="visibility: visible; animation-delay: 300ms; animation-name: fadeInUp;"><b>m-Commerce in India ,Mobile phones</b> </h4>
                            <div class=" wow fadeInUp text-left mt-4" data-wow-delay="600ms" style="visibility: visible; animation-delay: 600ms; animation-name: fadeInUp;">
                                <div class="comment more">
                                   Mobile phones are being used widely for commerce in India now. However, industry pundits say that this is only the tip of the iceberg and m-commerce has only scratched the surface.

Data shows that more users access internet over mobile rather than over their PCs/computers in India. More than 70% of the populace has access to mobile phones and this number is growing every day. Mobile phones are also easier to use for the not so technical savvy class such as rural people, grand-moms, grand papas and so on. They can transact on their mobile phones without much difficulty. The cost of doing transactions would also be much lower on mobile phones as compared to other mediums. India already has one of the lowest rates in this space.
All these factors favour explosive growth of m-commerce industry. Banking, payments and marketing industry are the leaders who have embraced this concept completely. Banks are offering their services over the mobile phones. Some providers are giving the option of making small payments from their phones where they usually require the users to maintain some balance with them. Different industry segments are using this medium widely for marketing their products and services. We’re seeing real estate, retail, banking, ISPs and some other service providers doing this. Other industry segments also need to take this platform seriously or they might be left behind by their competitors.

However, there are some challenges in this domain which hold up its growth. Mobile phones have a smaller screen as compared to other computing devices. Due to this mobile applications usually have fewer features. Security is another area which needs continuous innovation. People are reluctant to share details of their creditdebit cards over the phones. There’s more risk of the device getting stolen or hacked. Another challenge is fewer options or applications for this platform but this issue will disappear as more providers come forward to offer services for mobile phones.

To conclude, this is a very interesting area with lot of unexplored opportunities. The emerging economies offer more opportunities in this space and which ever service providers can crack these markets will leapfrog into the big league very fast.

However, we’ve also seen that even though the usage of open source software grew manifold, paid software also survived. While Linux users grew exponentially, Windows retained its numero uno position. Same holds true for other softwares also. There are instances where there’s a basic version which is free and a premium version for which users have to pay. In my opinion, the same may happen in the mobile apps segment. A large part of the apps may be free but users would be ready to pay for those in the niche areas. There might be basic apps which are free but those with advanced features would be able to charge a premium.

There’s space for everyone. The pie is so BIG !!!</div>
                                 <div class="item"> <img src="images/news_1.jpg" alt="" class="img-fluid"> </div>
                            </div>
                              <div class="border border-success text-white"></div>
                      </div>
                              </div>
                      </div>
                       <div class="col-md-6">
                            <h4 class="text-left mt-4 wow fadeInUp" data-wow-delay="300ms" style="visibility: visible; animation-delay: 300ms; animation-name: fadeInUp;"><b>Will Users continue to pay for Mobile Apps</b> </h4>
                            <div class=" wow fadeInUp text-left mt-4" data-wow-delay="600ms" style="visibility: visible; animation-delay: 600ms; animation-name: fadeInUp;">

<div class="comment more">
    There’s a debate going on amongst the technology pundits – will users pay for mobile apps ? This is a very pertinent question for this multi billion dollar industry. Why would developers continue to invest money if they can’t make users pay for it.

As per the current data, more than 90% of the mobile apps are free. This is a price which no one can beat. Who doesn’t like a free lunch? When users start getting good enough apps for free, it becomes very difficult to make them pay. We saw this in the case of desktops also.
When open source movement became strong enough, many users stopped using their enterprise softwares for which they were paying hefty amounts, and switched to open source alternatives. Open source offerings have been increasing at a rapid pace along with the community using them.

However, we’ve also seen that even though the usage of open source software grew manifold, paid software also survived. While Linux users grew exponentially, Windows retained its numero uno position. Same holds true for other softwares also. There are instances where there’s a basic version which is free and a premium version for which users have to pay. In my opinion, the same may happen in the mobile apps segment. A large part of the apps may be free but users would be ready to pay for those in the niche areas. There might be basic apps which are free but those with advanced features would be able to charge a premium.

There’s space for everyone. The pie is so BIG !!!
</div>

                    </div>
                              <div class="border border-success text-white"></div>
                                     <div class="row">
                    
                       <div class="col-md-12">
                           <h4 class="text-left mt-4 wow fadeInUp" data-wow-delay="300ms" style="visibility: visible; animation-delay: 300ms; animation-name: fadeInUp;"><b>Mobile Apps for Surveying , Conducting surveys</b></h4>
                            <div class=" wow fadeInUp text-left mt-4" data-wow-delay="600ms" style="visibility: visible; animation-delay: 600ms; animation-name: fadeInUp;">                                
                               <div class="comment more">Conducting surveys online has become a very big industry. Online surveys offer many advantages over traditional paper based surveys. Conducting suveys over mobile devices can be even more advantageous in today’s context. They are much faster to conduct and the results are quickly available.

They also work out to be cheaper. Surveys conducted over mobile devices are much more flexible as we can change
Mobile apps are another channel available for marketing. It is possible for app providers to push notifications or alerts to those who have downloaded their apps. Most of the times users have to take some actions when these notifications are received. Several options are available in mobile apps space which can be used for marketing. mSense is one such app which has been developed by ThoughtSpheres Technologies (http://thoughtspheres.com). the order of questions or we can have context based surveys. It’s also much easier to customize the look and feel of online surveys as compared to paper based ones. Scope of errors is also much lesser.

All these advantages have ensured that businesses in various domains are adopting these in a big way. Hotels, Restaurants, Hospitals, Educational institutes, Media & News are using this to get feedbacks on their services or products. Several companies are also using this to conduct employee satisfaction surveys. Surveys conducted on mobile phones allow users to respond any time from any place. Users may also feel more comfortable sharing details as they get lot of privacy.Several offerings are available from providers such as SurveyToGo, Survey Anyplace, SurveyPocket, SurveyMonkey which enable mobile surveys. However, what might be good is if we had a platform where users could do much more than answer questions. Users could have some discussions amongst themselves related to the surveys. They might also want to share some content such as audio or video files as their inputs. There might also be some interactive options such as chat.

We hope we’ll see these and more features in surveying apps soon.</div>
                                <div class="item"> <img src="images/news_2.jpg" alt="" class="img-fluid"> </div>
                            </div>
                              <div class="border border-success text-white"></div>
                      </div>
                        </div>
                      </div>
                        </div>               
                      </section>                        
            </div>
        <!--header end-->
        <div class="clearfix"></div>
     
      <div class="container">
                <div class="brand-carousel bottom50 owl-carousel owl-theme owl-loaded owl-drag">
                    <div class="owl-stage-outer">
                        <div class="owl-stage" style="transform: translate3d(-1596px, 0px, 0px); transition: all 0.25s ease 0s; width: 4560px;">
                            <div class="owl-item " style="width: 198px; margin-right: 30px;">
                                <div class="item"> <img src="images/logo_orimark.png" alt=""> </div>
                            </div>
                            <div class="owl-item " style="width: 198px; margin-right: 30px;">
                                <div class="item"> <img src="images/navedas.png" alt=""> </div>
                            </div>
                            <div class="owl-item " style="width: 198px; margin-right: 30px;">
                                <div class="item"> <img src="images/oneglint-logo.png" alt=""> </div>
                            </div>
                            <div class="owl-item " style="width: 198px; margin-right: 30px;">
                                <div class="item"> <img src="images/patanjali.png" alt=""> </div>
                            </div>
                            <div class="owl-item " style="width: 198px; margin-right: 30px;">
                                <div class="item"> <img src="images/pcm.png" alt=""> </div>
                            </div>
                            <div class="owl-item" style="width: 198px; margin-right: 30px;">
                                <div class="item"> <img src="images/save.png" alt=""> </div>
                            </div>
                        </div>
                    </div>
                    <div class="owl-nav disabled">
                        <button type="button" role="presentation" class="owl-prev"><span aria-label="Previous">‹</span></button>
                        <button type="button" role="presentation" class="owl-next"><span aria-label="Next">›</span></button>
                    </div>
                    <div class="owl-dots disabled"></div>
                </div>
            </div>
            </div>
<script>
$(document).ready(function() {
	var showChar = 100;
	var ellipsestext = "...";
	var moretext = "more";
	var lesstext = "less";
	$('.more').each(function() {
		var content = $(this).html();
		if(content.length > showChar) {
			var c = content.substr(0, showChar);
			var h = content.substr(showChar-1, content.length - showChar);
			var html = c + '<span class="moreelipses">'+ellipsestext+'</span>&nbsp;<span class="morecontent"><span>' + h + '</span>&nbsp;&nbsp;<a href="" class="morelink">'+moretext+'</a></span>';
			$(this).html(html);
		}

	});

	$(".morelink").click(function(){
		if($(this).hasClass("less")) {
			$(this).removeClass("less");
			$(this).html(moretext);
		} else {
			$(this).addClass("less");
			$(this).html(lesstext);
		}
		$(this).parent().prev().toggle();
		$(this).prev().toggle();
		return false;
	});
});
</script>
                     <!-- address Start -->
          
            <!-- Footer end -->
           <?php include 'footer.php';?>
<!-- Footer end -->
    </body>
</html>