<header id="home" class="">
            <nav class="navbar navbar-top navbar-expand-lg side-logo-nav nav-line bg-green">
                <div class="container-fluid">
                    <a href="index.php" title="Logo" class="logo">
                        <img src="images/logo.png"  class="logo-dark" alt="logo">
                        <img src="images/logo.png"  alt="logo" class="logo-light default">
                    </a>
                    <button class="navbar-toggler pull-right" data-toggle="collapse" data-target="#navbarTogglerDemo02" aria-controls="navbarTogglerDemo02" aria-expanded="false" aria-label="Toggle navigation">
                        <span><i class="fa fa-bars fa-1x"></i></span>
                    </button>
                    <div class="collapse navbar-collapse" id="navbarTogglerDemo02">
                        <ul class="navbar-nav ml-auto mt-lg-0 ">
                            <li class="nav-item active">
                                <a class="nav-link" href="index.php">Home <span class="sr-only">(current)</span></a>
                            </li>
                        <li class="nav-item dropdown">
                            <a class="nav-link dropbtn" >About Us</a>
                            <ul class="dropdown-content">    
                                <li><a href="about.php#wt-we-are" class="text-black ">Who We Are</a></li>
                               <li><a href="about.php#mission" class="text-black ">Mission & Vision</a></li>
                                <li><a href="about.php#our-team" class="text-black ">Our Team</a></li>
                            </ul>
                        
                    </li>
                            <li class="nav-item dropdown">
                                <a  class="nav-link dropbtn" >Business Unit</a>
                                 <ul class="dropdown-content">    
                                     <li><a href="digital-services.php#digital" class="text-black  ">Digital Services</a></li>
                               <li><a href="digital-services.php#intrprise" class="text-black  ">Enterprise IT</a></li>
                               <li><a href="digital-services.php#engineering" class="text-black ">Product Engineering</a></li>
                            </ul>
                            </li>
                             <li class="nav-item dropdown">
                            <a class="nav-link dropbtn" >Technologies</a>
                            <ul class="dropdown-content">    
                                <li><a href="augmented.php" class="text-black ">Augmented Reality</a></li>
                               <li><a href="iot.php" class="text-black ">IoT</a></li>
                               <li><a href="webplatform.php" class="text-black ">Web Platform</a></li>                                
                               <li><a href="databaseplatform.php" class="text-black ">Database Platform</a></li>
                               <li><a href="wbiplatform.php" class="text-black ">DW/BI Platform</a></li>
                               <li><a href="analytics.php" class="text-black ">Analytics</a></li>
                               <li><a href="javajeeplatform.php" class="text-black ">Java/JEE Platform</a></li>                                
                               <li><a href="microsoftplatform.php" class="text-black ">Microsoft Platform</a></li>
                               <li><a href="smartuiplatform.php" class="text-black ">Smart UI Platform</a></li>
                            </ul>
                    </li>
                               <li class="nav-item dropdown">
                        <a class="nav-link dropbtn">Resources</a>
                        <ul class="dropdown-content">    
                            <li><a href="news.php#wt-we-are" class="text-black">News</a></li>
                            <li><a href="blog.php#mission" class="text-black">Blog</a></li>
                        </ul>
                    </li> 
                            <li class="nav-item">
                                <a class="nav-link disabled" href="carrer.php">Careers</a>
                            </li>
                            <li class="nav-item">
                                <a class="nav-link disabled" href="contactus.php">Contact Us</a>
                            </li>
                        </ul>
                    </div>
                    <div class="social_icons">
                        <ul>
                            <li><a href="#." class="facebook_bg_hvr2"><i class="fa fa-facebook" aria-hidden="true"></i></a> </li>
                            <li><a href="#." class="twitter_bg_hvr2"><i class="fa fa-twitter" aria-hidden="true"></i></a> </li>
                            <li><a href="#." class="instagram_bg_hvr2"><i class="fa fa-instagram" aria-hidden="true"></i></a> </li>
                        </ul>
                    </div>
                </div>
            </nav>
        </header>
