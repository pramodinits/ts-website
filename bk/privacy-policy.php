<!DOCTYPE html>
<html lang="en">
    <head>
        <!-- Modernizr js - required -->
        <!-- Required meta tags -->
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
        <title>thoughtspheres</title>    
        <?php include 'header.php';?>
    </head>
    <body data-spy="scroll" data-target=".navbar" data-offset="90">
        <!--loader start-->
        <!--loader end-->

        <!--header start-->

         <?php include 'inner-nav.php';?>
        <div style="background-color:#fffff;" >
            <div class="container-fluid" >
                <div class="row ">
                    <div class="col-lg-12" style="padding-top:100px;">
                        <div class="privacy-box">
                        <h3 class="text-center m-0">PRIVACY POLICY</h3>
                        <p class="text-center"><b>At thoughtspheres.com, the privacy of our visitors is highly valued. The following privacy policy statement illustrates the manner in which the personal information procured by thoughtspheres.com is used.</b></p>
                        <hr/>
                     
                       <h4 class="text-capitalize mt-4 mb-4"> Log Files</h4>
<p>Thoughtspheres.com uses the log files. These log files embrace the information w.r.t the internet protocol (IP) addresses, type of browser, Internet Service Provider (ISP), date/time stamp, referring/exit pages, and number of clicks in order to analyze trends, administer the site, track visitor’s movement/usage around the site, and collects demographic information. Such information does not provide access to any information that is personally identifiable.
    </p>
         <h4 class="text-capitalize mt-4 mb-4">Cookies</h4>
<p>We use cookies with a view to source information regarding the visitors preferences, record individual user-specific usage pattern through user access or visit and thereby customize web page content as per visitors browser type or other information received by us through their browser. However, you shall have the option to disable cookies through your individual browser options. The specific details regarding the cookie management of the specific web browsers is accessible at the respective browsers' websites.
      </p>

    <h4 class="text-capitalize mt-4 mb-4"> Data recipients, transfer and disclosure of Personal Information:</h4>
<p>Thoughtspheres.com does not share your Personal Information with third parties for marketing purposes without seeking your prior permission. We will seek your permission prior to using or sharing Personal Information for any purpose beyond the requirement for which it was originally collected. We may share your Personal Information within ThoughtSpheres or with any of its subsidiaries, business partners, service vendors, authorized third-party agents, or contractors located in any part of the world for the purposes of data processing, storage, or to provide a requested service or transaction, after making sure that such entities are contractually bound by data privacy obligations. When required, ThoughtSpheres may disclose Personal Information to external law enforcement bodies or regulatory authorities, in order to comply with legal obligations. 
    If still have any query regarding our privacy policy, please feel free to contact us by email at contactus@thoughtspheres.com.</p>
                    </div>
                    </div>
                   
                </div>
            </div>
        </div>
        <!--header end-->
        <div class="clearfix"></div>
            <!-- address Start -->
          
            <!-- Footer end -->
           <?php include 'footer.php';?>
<!-- Footer end -->

    </body>
</html>