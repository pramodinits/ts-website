<!DOCTYPE html>
<html lang="en">
    <head>
        <!-- Modernizr js - required -->
        <!-- Required meta tags -->
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
        <title>thoughtspheres</title>    
        <?php include 'header.php';?>
    </head>
    <body data-spy="scroll" data-target=".navbar" data-offset="90">
        <!--loader start-->
        <!--loader end-->

        <!--header start-->

         <?php include 'inner-nav.php';?>
       
        <div style="background-color:#fffff;" >
            <div class="container" >
                  <section class="about" id="digital" >
               <div class="row">
                   <div class="col-lg-12 mt-5 text-center">
                            <h2 class="wow fadeInUp text-black font-weight-bold" data-wow-delay="300" style="visibility: visible; animation-name: fadeInUp;">NEWS</h2>
                            <div class="border-success text-white" style="border:solid 1px #28a745;"></div>
                            <h4 class="text-left mt-4 wow fadeInUp" data-wow-delay="300ms" style="visibility: visible; animation-delay: 300ms; animation-name: fadeInUp;"><b>PATANJALI’s Augmented Reality Mobile App Contract</b> </h4>
                            <div class=" wow fadeInUp text-left mt-4" data-wow-delay="600ms" style="visibility: visible; animation-delay: 600ms; animation-name: fadeInUp;">
                                
                                <p> ThoughtSpheres bags PATANJALI’s Augmented Reality Mobile App Contract

On 7th Jan 2017,PATANJALI, India’s leading retail company chose ThoughtSpheres as the technology partner for developing its product based Augmented Reality Mobile App</p>

                            </div>
                              <div class="border border-success text-white"></div>
                        </div>                   
                </div>
                      <div class="row">
                      <div class="col-md-6  border-right">
                            <h4 class="text-left mt-4 wow fadeInUp" data-wow-delay="300ms" style="visibility: visible; animation-delay: 300ms; animation-name: fadeInUp;"><b>FbStart Bootstrap Program</b> </h4>
                            <div class=" wow fadeInUp text-left mt-4" data-wow-delay="600ms" style="visibility: visible; animation-delay: 600ms; animation-name: fadeInUp;">
                              
                                <p>Selected for FB Start Bootstrap Program We are pleased to announce that, our ‘Inspect’ App got selected by FbStart for their Bootstrap program.</p>
                                                          
<span class="complete"> this is the 
complete text being shown</span>

<span class="more">more...</span>

                            </div>
                              <div class="border border-success text-white"></div>
                              <div class="row">
                                <div class="col-md-12 ">
                            <h4 class="text-left mt-4 wow fadeInUp" data-wow-delay="300ms" style="visibility: visible; animation-delay: 300ms; animation-name: fadeInUp;"><b>Mobile Applications Conference</b> </h4>
                            <div class=" wow fadeInUp text-left mt-4" data-wow-delay="600ms" style="visibility: visible; animation-delay: 600ms; animation-name: fadeInUp;">
                              
                                <p>Thoughtspheres displayed its Augmented Reality (AR) product mSense at Mobile Applications Conference (MAC) held in New Delhi on 4th May 2013.</p>
                                 <div class="item"> <img src="images/news_1.jpg" alt="" class="img-fluid"> </div>
                            </div>
                              <div class="border border-success text-white"></div>
                      </div>
                              </div>
                      </div>
                       <div class="col-md-6">
                            <h4 class="text-left mt-4 wow fadeInUp" data-wow-delay="300ms" style="visibility: visible; animation-delay: 300ms; animation-name: fadeInUp;"><b>Release of Inspect APP</b> </h4>
                            <div class=" wow fadeInUp text-left mt-4" data-wow-delay="600ms" style="visibility: visible; animation-delay: 600ms; animation-name: fadeInUp;">
                               
                                <p>Our first consumer app-Inspect available on Android platform its first consumer Mobile App ‘Inspect’ 
Thoughtspheres is pleased to announce that,   <span href="#demo" class="btn btn-link" data-toggle="collapse" style="color:#000 !important;">Read More...</span></p>
                                                               
  <div id="demo" class="collapse">
      (version 1.1.1) is now available for Android platform only. Visit http://inspect.co.in to know more details.
  </div>
                            </div>
                              <div class="border border-success text-white"></div>
                                     <div class="row">
                    
                       <div class="col-md-12">
                           <h4 class="text-left mt-4 wow fadeInUp" data-wow-delay="300ms" style="visibility: visible; animation-delay: 300ms; animation-name: fadeInUp;"><b>Article on Augmented Reality</b></h4>
                            <div class=" wow fadeInUp text-left mt-4" data-wow-delay="600ms" style="visibility: visible; animation-delay: 600ms; animation-name: fadeInUp;">                                
                                <p>Pioneered Augmented Reality based software application in Bhubaneswar.</p>
                                <div class="item"> <img src="images/news_2.jpg" alt="" class="img-fluid"> </div>
                            </div>
                              <div class="border border-success text-white"></div>
                      </div>
                        </div>
                      </div>
                        </div>               
                      </section>                        
            </div>
        <!--header end-->
        <div class="clearfix"></div>
     
      <div class="container">
                <div class="brand-carousel bottom50 owl-carousel owl-theme owl-loaded owl-drag">
                    <div class="owl-stage-outer">
                        <div class="owl-stage" style="transform: translate3d(-1596px, 0px, 0px); transition: all 0.25s ease 0s; width: 4560px;">
                            <div class="owl-item " style="width: 198px; margin-right: 30px;">
                                <div class="item"> <img src="images/logo_orimark.png" alt=""> </div>
                            </div>
                            <div class="owl-item " style="width: 198px; margin-right: 30px;">
                                <div class="item"> <img src="images/navedas.png" alt=""> </div>
                            </div>
                            <div class="owl-item " style="width: 198px; margin-right: 30px;">
                                <div class="item"> <img src="images/oneglint-logo.png" alt=""> </div>
                            </div>
                            <div class="owl-item " style="width: 198px; margin-right: 30px;">
                                <div class="item"> <img src="images/patanjali.png" alt=""> </div>
                            </div>
                            <div class="owl-item " style="width: 198px; margin-right: 30px;">
                                <div class="item"> <img src="images/pcm.png" alt=""> </div>
                            </div>
                            <div class="owl-item" style="width: 198px; margin-right: 30px;">
                                <div class="item"> <img src="images/save.png" alt=""> </div>
                            </div>
                        </div>
                    </div>
                    <div class="owl-nav disabled">
                        <button type="button" role="presentation" class="owl-prev"><span aria-label="Previous">‹</span></button>
                        <button type="button" role="presentation" class="owl-next"><span aria-label="Next">›</span></button>
                    </div>
                    <div class="owl-dots disabled"></div>
                </div>
            </div>
            </div>
<script>
     $(".more").toggle(function(){
    $(this).text("less..").siblings(".complete").show();    
}, function(){
    $(this).text("more..").siblings(".complete").hide();    
});            
</script>
                     <!-- address Start -->
          
            <!-- Footer end -->
           <?php include 'footer.php';?>
<!-- Footer end -->
    </body>
</html>