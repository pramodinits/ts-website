<!DOCTYPE html>
<html lang="en">
    <?php include 'header.php';?>
    <body data-spy="scroll" data-target=".navbar" data-offset="90">
        <!--loader start-->
        <!--loader end-->

        <!--header start-->
         <?php include 'inner-nav.php';?>

         <section class="" id="wt-we-are">
   
            <div class="container-fluid" >
                <div class="row ">
                    <div class="col-lg-7 mt-5 pl-5" style="padding-top:80px;">
                        <h3 class="wow fadeInUp " data-wow-delay="300" style="visibility: visible; animation-name: fadeInUp;">TS Tale </h3>
                        <div class="border border-success "></div>
                        <h5 class="mt-3 wow fadeInUp " data-wow-delay="300ms" style="visibility: visible; animation-delay: 300ms; animation-name: fadeInUp;">We develop apps and web platform and maintain the same!</h5>
                        <p class="mt-4 wow fadeInUp text-justify" data-wow-delay="400" style="visibility: visible; animation-name: fadeInUp;">Going Digital is pre-eminent not only for enterprise success but also to save the planet. We want to collaborate with you in taking this initiative forward. We facilitate the process of business transformation through automation adoption. That's the best way to meet the entrepreneurial challenges arising in the current business ecosystem. Execution of your IT requirement gets as easy as it could with TS around.</p>
                    </div>
                    <div class="col-lg-5 p-5">
                        <img src="images/aboout_banner.jpg" class="img-fluid">
                    </div>
                </div>
           
        </div>
         </section>
        <!--header end-->
        
    
            <section class="team" id="mission">
        <div class="blog mt-5" id="blog">
            <div class="container">
                <div class="row align-items-center">
                    <div class="col-lg-5 ">
                        <div class="img-hvr">
                            <img src="images/mission.png" alt="">
                        </div>
                    </div>
                    <div class="col-lg-7 ">
                        <h3 class="wow fadeInUp" data-wow-delay="300" style="visibility: visible; animation-name: fadeInUp;">Mission
                        </h3>
                        <div class="border border-success"></div>
                        <h5 class="mt-3 wow fadeInUp" data-wow-delay="300ms" style="visibility: visible; animation-delay: 300ms; animation-name: fadeInUp;">Our Focus</h5>
                        <p class="mt-2 wow fadeInUp" data-wow-delay="400" style="visibility: visible; animation-name: fadeInUp;">Leverage our skills in a collaborative manner to continually enhance our products and services to exceed customer expectations..</p>
                        <h3 class="wow fadeInUp" data-wow-delay="300" style="visibility: visible; animation-name: fadeInUp;">Vision
                        </h3>
                        <div class="border border-success"></div>
                        <h5 class="mt-3 wow fadeInUp" data-wow-delay="300ms" style="visibility: visible; animation-delay: 300ms; animation-name: fadeInUp;">Dominating Digital Space</h5>
                        <p class="mt-2 wow fadeInUp" data-wow-delay="400" style="visibility: visible; animation-name: fadeInUp;">To become an excellent hub of system design and technical competence.</p>
                    </div>
                </div>
                <div class="row align-items-center mt-5">
                    <div class="col-lg-5 order-lg-1 ">
                        <div class="img-hvr text-right">
                            <img src="images/value.png" class="mw-100" alt="">
                        </div>
                    </div>
                    <div class="col-lg-7 ">
                        <h3 class="wow fadeInUp" data-wow-delay="300" style="visibility: visible; animation-name: fadeInUp;">Values </h3>
                        <div class="border border-success"></div>
                        <h5 class="mt-3 wow fadeInUp" data-wow-delay="300ms" style="visibility: visible; animation-delay: 300ms; animation-name: fadeInUp;">Customer Centricity</h5>
                        <p class="mt-2 wow fadeInUp" data-wow-delay="400" style="visibility: visible; animation-name: fadeInUp;">Customer is at the core either – External or Internal. We deliver measurable outcome against customer's investment and strive for excellence holistically.</p>
                        <h5 class="mt-3 wow fadeInUp" data-wow-delay="300ms" style="visibility: visible; animation-delay: 300ms; animation-name: fadeInUp;">Innovation</h5>
                        <p class="mt-2 wow fadeInUp" data-wow-delay="400" style="visibility: visible; animation-name: fadeInUp;">Our forte is acquisition and implementation of new capabilities which allows competitive edge thus assuring leadership in the arena.</p>
                        <h5 class="mt-3 wow fadeInUp" data-wow-delay="300ms" style="visibility: visible; animation-delay: 300ms; animation-name: fadeInUp;">Respect & Trust</h5>
                        <p class="mt-2 wow fadeInUp" data-wow-delay="400" style="visibility: visible; animation-name: fadeInUp;">We appreciate diversity, respect individuality and encourage cooperation to nurture the shared vision.</p>
                        <h5 class="mt-3 wow fadeInUp" data-wow-delay="300ms" style="visibility: visible; animation-delay: 300ms; animation-name: fadeInUp;">Professionalism</h5>
                        <p class="mt-2 wow fadeInUp" data-wow-delay="400" style="visibility: visible; animation-name: fadeInUp;">Being a value driven organization, integrity and professionalism assume paramount importance in our business operations.</p>
                    </div>
                </div>
            </div>
        </div>
        </section>
           <section class="team" id="our-team">
            <div class="bg-green p-5 mt-5">
                <div class="container">
                    <h3 class="wow fadeInUp text-center text-black" data-wow-delay="300" style="visibility: visible; animation-name: fadeInUp;">MEET OUR TEAM </h3>
                    <div class="border-ctm border-dark"></div>
                    <div class="col-md client">
                        <div class="text-left mt-4"> </div>
                        <p class="m-auto font-weight-light"> <i class="fa fa-quote-left text-left pr-2"></i> It is group of dynamic professionals who bring to table a rich industry experience.Keeping their team motivated with a challenging environment and acknowledgement of their potential has manifested itself in the shape of numerous innovative products and solutions resulting in a long list of pleased clientele. Highly talented and skilled in their respective domain they guide the team effortlessly thus eliminating the scope of any work pressure hence ensuring a smooth work life balance. A management team which boasts to have a bunch of happy geeks in their team too excited to be a part of the company!<i class="fa fa-quote-right text-right pl-2"></i></p>
                    </div>
                </div>
            </div>
           </section>>

            <!-- address Start -->
            <?php include 'footer.php'; ?>
            
    </body>


</html>