<!DOCTYPE html>
<html lang="en">
    <head>
        <!-- Modernizr js - required -->
        <!-- Required meta tags -->
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
        <title>thoughtspheres</title>    
        <?php include 'header.php';?>
    </head>
    <body data-spy="scroll" data-target=".navbar" data-offset="90">
        <!--loader start-->
        <!--loader end-->

        <!--header start-->

         <?php include 'inner-nav.php';?>
       
        <div style="background-color:#fffff;" >
            <div class="container" >
                  <section class="about" id="digital" >
               <div class="row">
                    <div class="col-lg-12 mt-5 ">
                        <h3 class="wow fadeInUp text-black" data-wow-delay="300" style="visibility: visible; animation-name: fadeInUp;">DIGITAL SERVICES </h3>
                        <div class="border border-success text-white"></div>
                        <h5 class="mt-3 wow fadeInUp" data-wow-delay="300ms" style="visibility: visible; animation-delay: 300ms; animation-name: fadeInUp;"><b>Develop, Disrupt, Disperse with Digital</b> </h5>
                        <div class=" wow fadeInUp" data-wow-delay="600ms" style="visibility: visible; animation-delay: 600ms; animation-name: fadeInUp;">
                    TS Digital Services is comprised of TS Synergy,TS Mobility and TS Analytics which aim at extending seamless consumers & enterprise experience. TS Synergy provides a comprehensive marketing solution ranging from creative design to digital campaigns utilizing our expertise in the field of content and social media to e-learning packages for enhanced customer engagement and consequently business generation. TS Mobility claims exquisite authority,having created Innovative Mobility solutions for numerous clients.We have expertise in the niche technology called AR . TS Analytics team possesses superior dexterity in helping the enterprise enjoy leading market position through critical data insights and analysis .
                </div>
                    </div>
                    <div class="col-lg-12 mt-5">
                        <img src="images/Digital-services.jpg" class="img-fluid " alt="">
                    </div>
                </div>
                      </section>                
                  <section class="about" id="intrprise">
                <div class="row">
                    <div class="col-lg-12" >
                        <h3 class="wow fadeInUp text-black" data-wow-delay="300" style="visibility: visible; animation-name: fadeInUp;">ENTERPRISE IT </h3>
                        <div class="border border-success text-white"></div>
                        <h5 class="mt-3 wow fadeInUp" data-wow-delay="300ms" style="visibility: visible; animation-delay: 300ms; animation-name: fadeInUp;">Fostering enterprises through smart solutions</h5>
                        <div class="wow fadeInUp" data-wow-delay="600ms" style="visibility: visible; animation-delay: 600ms; animation-name: fadeInUp;">
                    In the current scenario every organization requires smart, connected, secured and data driven solutions to have a remarkable presence and establish competitive advantage to claim dominant market share. We design smart and cost effective solutions for Enterprises through our innovation which nutures a sustainable business model and enhances Operational Efficiency. Using various frameworks, tools & methodologies we excel in creating robust business solutions.
                </div>
                    </div>
                    <div class="col-lg-12">
                        <img src="images/tech-mobility.png" class="img-fluid">
                    </div>
                </div>
            </section>
                <section class="about" id="engineering">
                   <div class="row " >
                    <div class="col-lg-12 ">
                        <h3 class="wow fadeInUp text-black" data-wow-delay="300" style="visibility: visible; animation-name: fadeInUp;">Product-engineering </h3>
                        <div class="border border-success text-white"></div>
                        <h5 class="mt-3 wow fadeInUp" data-wow-delay="300ms" style="visibility: visible; animation-delay: 300ms; animation-name: fadeInUp;">Crafting innovation through cutting edge technology</h5>
                        <div class="wow fadeInUp" data-wow-delay="600ms" style="visibility: visible; animation-delay: 600ms; animation-name: fadeInUp;">
                   <p>At ThoughtSpheres, we take pride in creating ground-breaking products for Enterprise as well as Consumers. Our Product Engineering Team demonstrates next generation technology skills. Deep understanding of critical business processes and consumer behavior allows us to offer best possible platform/solutions with extraordinary ease. Our core competence lies in the following areas: Mobility, Cloud, Social Solutions & Analytics.</P
                   <p>We undertake outsourced product design and development. Adopting a strictly professional approach TS collaborates with clients to build products across multiple domains to help them attain competitive advantage.</p>
                </div>
                    </div>
                    <div class="col-lg-12">
                        <img src="images/Product-Engineering-inner.jpg" class="img-fluid">
                    </div>
                </div>
            </section>            
            </div>
        <!--header end-->
        <div class="clearfix"></div>
       <section class="counter-bg pb-0">
            <div class="container">
                <div class="row">
                    <div class="col-lg-12">
                        <div class="wow fadeInUp" data-wow-delay="400ms" style="visibility: visible; animation-delay: 400ms; animation-name: fadeInUp;"> 
                         
                        </div>
                    </div>
                    <div class="col-lg-12 pr-4 wow fadeInRight" style="visibility: visible; animation-name: fadeInRight;">
                        <div class="img-hvr "></div>
                        <img src="images/ourclient.png" class=" pull-right" alt="">
                    </div>
                </div>
            </div>
        </section>
      <div class="container">
                <div class="brand-carousel bottom50 owl-carousel owl-theme owl-loaded owl-drag">
                    <div class="owl-stage-outer">
                        <div class="owl-stage" style="transform: translate3d(-1596px, 0px, 0px); transition: all 0.25s ease 0s; width: 4560px;">
                            <div class="owl-item " style="width: 198px; margin-right: 30px;">
                                <div class="item"> <img src="images/logo_orimark.png" alt=""> </div>
                            </div>
                            <div class="owl-item " style="width: 198px; margin-right: 30px;">
                                <div class="item"> <img src="images/navedas.png" alt=""> </div>
                            </div>
                            <div class="owl-item " style="width: 198px; margin-right: 30px;">
                                <div class="item"> <img src="images/oneglint-logo.png" alt=""> </div>
                            </div>
                            <div class="owl-item " style="width: 198px; margin-right: 30px;">
                                <div class="item"> <img src="images/patanjali.png" alt=""> </div>
                            </div>
                            <div class="owl-item " style="width: 198px; margin-right: 30px;">
                                <div class="item"> <img src="images/pcm.png" alt=""> </div>
                            </div>
                            <div class="owl-item" style="width: 198px; margin-right: 30px;">
                                <div class="item"> <img src="images/save.png" alt=""> </div>
                            </div>
                        </div>
                    </div>
                    <div class="owl-nav disabled">
                        <button type="button" role="presentation" class="owl-prev"><span aria-label="Previous">‹</span></button>
                        <button type="button" role="presentation" class="owl-next"><span aria-label="Next">›</span></button>
                    </div>
                    <div class="owl-dots disabled"></div>
                </div>
            </div>
                     <!-- address Start -->
          
            <!-- Footer end -->
           <?php include 'footer.php';?>
<!-- Footer end -->
    </body>
</html>