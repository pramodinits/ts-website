<!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
        <title>thoughtspheres</title>    
        <?php include 'header.php'; ?>
    </head>
    <body data-spy="scroll" data-target=".navbar" data-offset="90">
        <?php include 'inner-nav.php'; ?>
        <div style="background-color:#fffff;" >
            <div class="container" >
                <div class="row ">
                    <div class="col-lg-6 mt-2 " style="padding-top:100px;">
                        <h3 class="wow fadeInUp text-black" data-wow-delay="300" style="visibility: visible; animation-name: fadeInUp;">Java/JEE Platform</h3>
                        <div class="border border-success text-white"></div>
                        <h5 class="mt-3 wow fadeInUp" data-wow-delay="300ms" style="visibility: visible; animation-delay: 300ms; animation-name: fadeInUp;"><p>We have a very strong and dedicated team in Java/Java EE platform. Wrapped with huge experience to handle complicated Enterprise Web Applications / Products under Java EE platform,TS Java team is focused on coding standards, architecture, design patterns and reusable frameworks owning excellent skills working in weblogic, websphere, Sun JavaEE, JBoss and Tomcat server..</p>
                            Capabilities:</h5>
                        <li> COE for java/Java EE Technologies and related open source frameworks</li>
                        <li> Expertise in Java EE components like Servlet, JSP and EJB etc</li>
                        <li> Strong Skills in JSF framework</li>
                        <li> Expertise in third party open source frameworks like Struts, Spring and Hibernate</li>
                        <li> Strong understanding of Java EE architecture and design patterns</li>
                        <li> Expertise in OS GIS , SOA and Web Services</li>
                        <li> Expertise in Agile Development Methodologies</li>

                    </div>
                    <div class="col-lg-6 mt-5 pt-5 pb-5">
                        <img src="images/tech-mobility.png" class="img-fluid" style="margin-top: 30%;">
                    </div>
                </div>
            </div>
        </div>
        <!--header end-->
        <div class="clearfix"></div>
        <section class="counter-bg pb-0">
            <div class="container">
                <div class="row">
                    <div class="col-lg-8">
                        <h2 class="wow fadeInUp" data-wow-delay="400ms" style="visibility: visible; animation-delay: 400ms; animation-name: fadeInUp;">Develop, Disrupt, 
                            Disperse with Digital </h2>
                        <p class="pt-3 wow fadeInUp" data-wow-delay="500ms" style="visibility: visible; animation-delay: 500ms; animation-name: fadeInUp;">TS Digital Services is comprised of TS Synergy,TS Mobility and TS Analytics which aim at extending seamless consumers &amp; enterprise experience. </p>
                    </div>
                    <div class="col-lg-4 pr-4 wow fadeInRight" style="visibility: visible; animation-name: fadeInRight;">
                        <div class="img-hvr "></div>
                        <img src="images/ourclient.png" class=" pull-right" alt="">
                    </div>
                </div>
            </div>
        </section>
        <div class="container">
            <div class="brand-carousel bottom50 owl-carousel owl-theme owl-loaded owl-drag">
                <div class="owl-stage-outer">
                    <div class="owl-stage" style="transform: translate3d(-1596px, 0px, 0px); transition: all 0.25s ease 0s; width: 4560px;">
                        <div class="owl-item " style="width: 198px; margin-right: 30px;">
                            <div class="item"> <img src="images/logo_orimark.png" alt=""> </div>
                        </div>
                        <div class="owl-item " style="width: 198px; margin-right: 30px;">
                            <div class="item"> <img src="images/navedas.png" alt=""> </div>
                        </div>
                        <div class="owl-item " style="width: 198px; margin-right: 30px;">
                            <div class="item"> <img src="images/oneglint-logo.png" alt=""> </div>
                        </div>
                        <div class="owl-item " style="width: 198px; margin-right: 30px;">
                            <div class="item"> <img src="images/patanjali.png" alt=""> </div>
                        </div>
                        <div class="owl-item " style="width: 198px; margin-right: 30px;">
                            <div class="item"> <img src="images/pcm.png" alt=""> </div>
                        </div>
                        <div class="owl-item" style="width: 198px; margin-right: 30px;">
                            <div class="item"> <img src="images/save.png" alt=""> </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <?php include 'footer.php'; ?>
    </body>


</html>