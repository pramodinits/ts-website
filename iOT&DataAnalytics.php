<!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
        <title>thoughtspheres</title>    
        <?php include 'header.php'; ?>
    </head>
    <body data-spy="scroll" data-target=".navbar" data-offset="90" >
        <?php include 'inner-nav.php'; ?>
        <div style="background-color:#fffff;" >
            <div class="iot-background"> 
                <div class="container" >
                    <div class="row fill-main">
                        <div class="col-lg-6 mt-2 "></div>                  
                        <div class="col-lg-6 mt-2  text-center">
                            <h3 class="wow fadeInUp text-uppercase" data-wow-delay="400ms" style="font-weight: 900; visibility: visible; animation-delay: 400ms; animation-name: fadeInUp;">IOT & Data <span class="text-ray">Analytics</span></h3>
                            <div class="ctm-border"></div>
                            <h5 class="text-center mt-4 wow fadeInUp" data-wow-delay="300ms" style="visibility: visible; animation-delay: 300ms; animation-name: fadeInUp;"><b>Let Ts guide (Consulting), develop & Launch it for you</b> </h5>
                            <h3 class="mt-5 text-success">Why TS ?</h3>                       
                            <h5 class="text-center  wow fadeInUp" data-wow-delay="300ms" style="visibility: visible; animation-delay: 300ms; animation-name: fadeInUp;"><b>As it’s a easy job for us across all Platform</b> </h5>
                            <div class="btn-group text-center mt-4" role="group" aria-label="Basic example">
                                <button type="button" class="btn btn-naviblue btn-radius btn-lg">Android</button>
                                <button type="button" class="btn btn-success btn-radius btn-lg ml-4 mr-4 pl-4 pr-4">IOS</button>
                                <button type="button" class="btn btn-naviblue btn-radius btn-lg">Hybrid</button>
                            </div>
                        </div>
                    </div>
                    <div class="clearfix"></div>
                  
                </div>
            </div>
            <div class="container">
              <div class="row">
                        <div class="col-lg-12">                                                
                            <h5 class="wow fadeInUp" data-wow-delay="300ms" style="visibility: visible; animation-delay: 300ms; animation-name: fadeInUp;"><p class="text-justify">IoT,the latest buzz of the internet world, connects smart objects to the Internet. The IoT is soon going to serve as a transformational tool which would turn a large number of enterprises into digital businesses.IoT shall act as a breeding ground for new business models,would ensure enhanced productivity and shall lead to creation of new modes of revenue generation.According to a study by Gartner, by 2020 , 25 billion Internet-connected things will to lead to generation of $2 trillion of economic benefit globally. ThoughtSpheres possess a thoroughly equipped and enthusiastic team for this most current segment thus proudly aspires to be competent carrier of such extraordinary growth which is sure to be witnessed by the IT world via IoT.</p></h5>                  
                        </div>
                    </div>
            </div>
        </div>
        <!--header end-->
        <div class="clearfix"></div>
        <?php include 'ourclient.php'; ?>  
        <?php include 'footer.php'; ?> 
    </body>
</html>