<?php include 'header.php'; ?> 
<?php include 'nav.php'; ?>  

<link href="assets/css/docs.min.css" rel="stylesheet">
<link href="assets/css/style.css" rel="stylesheet">
<script src="assets/js/docs.min.js"></script>
<script src="assets/js/script.js"></script>

<div id="fullpage">
    <div class="section " id="section0" style="display: none;">
        <div class="marshall-container"> 
            <a href="index.php" title="Logo" class="logo scroll">
                <img src="images/logo.png" alt="logo" class="logo-light default img-fluid" style="position:absolute; z-index: 99999; margin:0 auto; left:0; right:0; width: 190px;">
            </a>     
            <div class="marshall-col-12 marshall-col-content align-center">
                <div id="triangle_canvas" class="mrs_canvas mrs_particles_canvas fill-main">
                    <canvas id="particle_3d" width="900" height="520" ></canvas>
                </div>
                <div class="container">
                    <h1 style="position: absolute; left:0; right:0; z-index: 9999; color: white; font-weight:bold; margin:0 auto; bottom:12%;; font-family:Roboto, sans-serif; font-size:40px;">Get Digitally Decked Up With Us - TS</h1>
                    <h3 id="type_it_slider" class="raleway swiper-slider-heading" style="position: absolute; left:0; right: 0; z-index: 9999; color: white; font-weight:normal; margin:0 auto; bottom:5%;"><i class="ti-placeholder" style="display:inline-block;width:0;line-height:0;overflow:hidden;">.</i><span style="display:inline;position:relative;font:inherit;color:inherit;" class="ti-container">Best Suitable for <strong style="font-weight: 600">Creative Agenc</strong></span><span style="display: inline; position: relative; font: inherit; color: inherit; opacity: 0.0649081;" class="ti-cursor">|</span></h3>
                </div>
            </div>
        </div>
    </div>
    <div class="section" id="section0">
        <div class="marshall-container">
            <a href="index.php" title="Logo" class="logo scroll">
                <img src="images/logo.png" alt="logo" class="logo-light default img-fluid" style="position:absolute; z-index: 99999; margin:0 auto; left:0; right:0; width: 190px;">
            </a>
            <div class="marshall-col-12 marshall-col-content align-center">
                <div id="photosphere" style="height: 650px;"></div>
            </div>
<!--            <div class="container">
                    <h1 style="position: absolute; left:0; right:0; z-index: 9999; color: white; font-weight:bold; margin:0 auto; bottom:73%;; font-family:Roboto, sans-serif; font-size:35px;">Get Digitally Decked Up With Us - TS</h1>
                    <h3 id="type_it_slider" class="raleway swiper-slider-heading" style="position: absolute; left:0; right: 0; z-index: 9999; color: white; font-weight:normal; margin:0 auto; bottom:73%;">
                        <i class="ti-placeholder" style="display:inline-block;width:0;line-height:0;overflow:hidden;">.</i>
                        <span style="display:inline;position:relative;font:inherit;color:inherit;" class="ti-container">
                            Best Suitable for <strong style="font-weight: 600">Creative Agenc</strong>
                        </span>
                        <span style="display: inline; position: relative; font: inherit; color: inherit; opacity: 0.0649081;" class="ti-cursor">|</span>
                    </h3>
                </div>-->
            <div class="container">
                <h1 class="banner-content" data-wow-delay="400ms">Get Digitally Decked Up With Us - TS...
                <br/>
                <span style="color: #00dcb4;">ThoughtSpheres</span>
                </h1>
            </div>
        </div>
    </div>
    <section class="fill-main" id="about">
        <div class="container">
            <div class="row">
                <div class="col-lg-4  wow fadeInLeft">
                    <div class="img-hvr "></div>
                    <img src="images/aboutus.png" class="img-fluid" alt="">                        
                </div>
                <div class="col-lg-8">
                    <h3 class="wow fadeInUp text-uppercase " data-wow-delay="400ms" style="font-weight: 900;">We create smart <span class="text-ray">tech-savvy</span> environment</h3>
                    <p class="pt-3 wow fadeInUp text-justify" data-wow-delay="500ms">We are a leading edge IT and consulting firm possessing a strong leadership team obsessed with a technology-driven approach loaded with rich global corporate experience. We aspire to be partners in overcoming the emerging technical challenges faced by the Enterprise Community and IT Users globally. We deal with Actuate Technology & Consulting Services by leveraging our unparalleled expertise and analytical insights. Our sphere of influence predominantly lies in the IT Landscape as we design smart, innovative products as well simple yet exceedingly effective services for enriched business transformation.</p>
                </div>
            </div>
        </div>
    </section>
    <section class="fill-main bg-secondary" id="about" style="display: none;">
        <div class="container ">
            <div class="row">
                <div class="col-md-4 pr-4 order-md-2 wow fadeInRight">
                    <div class="img-hvr "></div>
                    <img src="images/businessunits.png" class="img-fluid pull-right" alt="">
                </div>
                <div class="col-md-8  order-md-1 text-center">
                    <h3 class="wow fadeInUp text-uppercase mb-5 mt-5" data-wow-delay="400ms" style="font-weight: 900;"><b>Our <span class="text-ray">Three</span> Business Units </b>
                    </h3>
                    <div class="row">
                        <div class="col-md-4 col-sm-6">
                            <a href="digital-services.php">
                                <img src="images/digital_services.png" class="img-fluid">
                            </a>
                            <h5 class="mt-4"><b>Digital Services</b></h5>
                            <div>Develop, Disrupt, Disperse<br>with Digital</div>
                        </div>
                        <div class="col-md-4 col-sm-6">
                            <a href="enterprise-it.php">
                                <img src="images/enterprise_it.png" class="img-fluid">
                            </a>
                            <h5 class="mt-4"><b>Enterprise-IT</b></h5>
                            <div>Fostering enterprises through <br>smart solutions</div>
                        </div>
                        <div class="col-md-4 col-sm-6">
                            <a href="product-engineering.php">
                                <img src="images/product_engeering.png" class="img-fluid">
                            </a>
                            <h5 class="mt-4"><b>Product Engineering</b></h5>
                            <div>Crafting innovation through <br>cutting edge technology</div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <section class="fill-main " id="about">
        <div class="container">
            <div class="row">
                <div class="col-md-3">
                    <img src="images/technology_capabilities.png" alt="Skytsunami" class=" img-fluid d-block mt-3"/>
                </div>
                <div class="col-md-9 bg-mobile">
                    <div class="row">
                        <div class="col-md-6 col-sm-4 text-center">
                            <div class="avatar">
                                <a href="augmented.php">
                                    <img src="images/ai.png" alt="AI/ML" class=" img-left img-shadow"/>
                                </a>
                            </div>
                        </div>
                        <div class="col-md-4 col-sm-4 text-center">
                            <div class="avatar">
                                <a href="augmented.php">
                                    <img src="images/Cyber-Security.png" alt="Cyber Security" class=" img-left img-shadow"/>
                                </a>
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-6 col-sm-4 text-center">
                            <div class="avatar">
                                <a href="augmented.php">
                                    <img src="images/bigdata.png" alt="Big Data" class="img-right img-shadow"/>
                                </a>
                            </div>
                        </div>
                        <div class="col-md-6 col-sm-4 text-center">
                            <div class="avatar">
                                <a href="augmented.php">
                                    <img src="images/arvr.png" alt="AR/VR" class="img-right img-shadow"/>
                                </a>
                            </div>
                        </div>

                    </div>
                </div>                    
            </div>
        </div>
    </section> 
</div>

<!-- @formatter:off -->
<link rel="stylesheet" href="assets/css/photo-sphere-viewer.min.css">
<script src="https://cdn.jsdelivr.net/npm/three@0.99.0/build/three.min.js"></script>
<script src="https://cdn.jsdelivr.net/npm/promise-polyfill@8.1.0/dist/polyfill.min.js"></script>
<script src="https://cdn.jsdelivr.net/npm/dot@1.1.2/doT.min.js"></script>
<script src="https://cdn.jsdelivr.net/npm/uevent@1.0.0/uevent.min.js"></script>
<script src="assets/js/photo-sphere-viewer.min.js"></script>
<!-- @formatter:on -->
<script>
    var PSV = new PhotoSphereViewer({
        panorama: "assets/panorama.jpg",
        container: 'photosphere',
        //    caption: 'Bryce Canyon National Park <b>&copy; Mark Doliner</b>',
        loading_img: "https://photo-sphere-viewer.js.org/assets/photosphere-logo.gif",
        //    navbar: 'autorotate zoom download caption fullscreen',
        //    default_fov: 70,
        //    default_lat: 0.3,
        mousewheel: false,
        mousemove_hover: true
        //    touchmove_two_fingers: true,
        
    });
</script>
<?php include 'footer.php'; ?>