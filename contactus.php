<?php include 'header.php'; ?>
<?php include 'inner-nav.php'; ?>
<section class="contact-form circle-bg2 wow fadeInUp" data-wow-delay="300" style="visibility: visible; animation-name: fadeInUp;">
    <div class="section contact-us-form fp-section active fp-completely" data-anchor="1">
        <div class="container">
            <div class="testimonials success-Story-app home-contactus get-in-touch">
                <div class="contactdtls">
                    <div class="row">
                        <div class="col-md-6 col-12 order-md-2">                              
                            <div class="home-contactus-con">
                                <h3 class="text-capitalize" style="color: #15496e;">Get In Touch </h3>
                                <form name="businessinquiryForm" action="contact.php" id="businessinquiryForm" method="post" accept-charset="utf-8" enctype="multipart/form-data">
                                    <div class="row">
                                        <div class="form-group col-md-6">
                                            <input type="text" name="Name" class="form-control" required="" placeholder="Name">
                                        </div>
                                        <div class="form-group col-md-6">
                                            <input type="text" name="Email" class="form-control" required="" placeholder="Last Name">
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="form-group col-md-6 ">
                                            <input type="text" name="Name" class="form-control" required="" placeholder="E-mail">
                                        </div>
                                        <div class="form-group col-md-6 ">
                                            <!-- <div class="country-number"> -->
                                            <input class="form-control" id="mobile-number" placeholder="Contact Number" name="phone_number" type="tel">
                                            <!-- </div> -->
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="form-group col-md-12 ">
                                            <textarea class="form-control" name="Message" rows="2" required="" placeholder="Message"></textarea>
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="col-md-4 ">
                                            <button type="submit" class="btn btn-info">Send</button>
                                        </div>
                                    </div>
                                </form>
                            </div>                                
                        </div>
                        <div class="col-md-6 col-12 order-md-1">                                
                            <div class="m-0 wow fadeInUp " data-wow-delay="300" style="visibility: visible; animation-name: fadeInUp;">
                                <h3 class="text-capitalize" style="color: #15496e;">Contact Us </h3>
                                <h4 class="mt-5" style="color: #10a58f !important;">India Office</h4>
                                <p class="pb-4">
                                    First Floor, Sai Saraswati Complex, 81, District Centre <br>
                                    Niladri Vihar, Chandrashekharpur, Bhubaneswar
                                </p>
                                <h4 class="mt-3" style="color: #10a58f !important;">UK Office</h4>
                                <p class="pb-4 ">15 Tamer Road, Sleaford Engand NG34 7GS </p>
                                <h4 class="text-capitalize" style="color: #10a58f !important;">General Support</h4>
                                <p class="pb-4 ">
                                    <a href="mailto:contactus@thoughtsphres.com">
                                        contactus@thoughtsphres.com
                                    </a>
                                </p> 
                            </div>
                        </div>     

                    </div>
                </div>



            </div>
        </div>
    </div>
</div>
</section>
<?php include 'footer.php'; ?>