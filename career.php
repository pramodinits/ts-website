<?php include 'header.php'; ?>
<?php include 'inner-nav.php'; ?>
<div class="career-banner">
    <img class="img-fluid" src="images/career_banner.jpg">
    <div class="career-header text-center">
        <h2 class="wow fadeInUp text-black font-weight-bold" data-wow-delay="300" style="visibility: visible; animation-name: fadeInUp;">CAREERS</h2>
        <img class="img-fluid" src="images/gradient_news.png"/>
        <!---->
    </div>
</div>
<div class="container">
    <div class="about" id="career">
        <div class="row">
            <div class="col-lg-12 mt-4 text-center">
<!--                    <h2 class="wow fadeInUp text-black font-weight-bold" data-wow-delay="300" style="visibility: visible; animation-name: fadeInUp;">CAREERS</h2>
                    <div class="text-center">
                        <img class="img-fluid" src="images/gradient.png"/>
                    </div>-->
                <h4 class="text-left mt-4 wow fadeInUp" data-wow-delay="300ms" style="visibility: visible; animation-delay: 300ms; animation-name: fadeInUp;"></h4>
                <div class=" wow fadeInUp text-left mt-4" data-wow-delay="600ms" style="visibility: visible; animation-delay: 600ms; animation-name: fadeInUp;">
                    <h4 class="text-center">To be the architect of a smart digital space
                    </h4>
                    <h5 class="text-center">
                        <span class="font-weight-light">Send your CV to <a href="mailto:hr@thoughtspheres.com" style="color: #296278;">hr@thoughtspheres.com</a></span>
                    </h5>
                </div>
            </div>

            <div class="card p-2 border-0 mt-3">
                <div class="row">
                    <div class="col-lg-4 col-xs-12 col-sm-12">
                        <img src="images/career_1.jpg" class="img-fluid" alt=""> 
                    </div>
                    <div class="col-lg-8 col-xs-12 col-sm-12">
                        <div class="row pt-4 mt-4">
                            <h4 class="font-weight-bold">Innovative Approach</h4>
                            <p>Yes we do transform the business environment through disruptive technologies. Gulping in new capabilities and throwing out inventive solutions is a norm at TS.</p>
                        </div>
                        <div class="row pt-4 mt-4">
                            <h4 class="font-weight-bold">Extraordinary Growth Opportunity</h4>
                            <p>If you have the fire then we let you ignite (not literally though).Anybody with great potential is bound to take flight here. (Taking flights for successful employees is quite literally possible)</p>
                        </div>
                    </div>
                </div>
            </div>

            <div class="clearfix"></div>

            <div class="card p-2 border-0 mt-3">
                <div class="row">
                    <div class="col-lg-8 col-sm-12 col-xs-12">
                        <div class="row pt-4 mt-4">
                            <h4 class="font-weight-bold">Excellent Leadership Team</h4>
                            <p>We have a strong and diverse leadership team who are but obviously exceptionally endowed; in addition to that they have a passion to foster the hidden talent as expected from true leaders.
                            </p>
                        </div>
                        <div class="row pt-4 mt-4">
                            <h4 class="font-weight-bold">Open & Transparent</h4>
                            <p>At TS, we are open to feedback and transparent in our approach.So walk into our premises and you can expect a lot of open,peer to peer and associates to leadership team discussions. Can we say it is luck that yet we are able to deliver projects on schedule? ;)</p>
                        </div>
                    </div>
                    <div class="col-lg-4 col-sm-12 col-xs-12">
                        <img src="images/career_2.jpg" class="img-fluid" alt=""> 
                    </div>
                </div>
            </div>
            <div class="clearfix"></div>

            <div class="card p-2 border-0 mt-3">
                <div class="row">
                    <div class="col-lg-4 col-sm-12 col-xs-12">
                        <img src="images/career_3.jpg" class="img-fluid" alt=""> 
                    </div>
                    <div class="col-lg-8 col-sm-12 col-xs-12">
                        <div class="row pt-4 mt-4">
                            <h4 class="font-weight-bold">Work Life Balance</h4>
                            <p>We certainly can’t afford to ignore work life balance in this ‘we live life only once’ era. ‘Smart Work’ is what we strongly advocate while work from home flexibility is provided as and when required. Thanks to our employee engagement team recreation activities sometimes delay our departure timings.
                            </p>
                        </div>
                        <div class="row pt-4 mt-4">
                            <h4 class="font-weight-bold">Equal Employment Opportunity</h4>
                            <p>‘Equal Employment opportunity’ is the need of the hour and TS is bound to pitch in. We believe in skills and talent irrespective of the gender. Of course, the job aspirant/ employee is expected to prove one’s worth to grab the opportunities.
                            </p>
                        </div>
                    </div>
                </div>
            </div>
        </div>                   
    </div>             
</div>
<?php include 'footer.php'; ?>