<!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
        <title>thoughtspheres</title>    
        <?php include 'header.php'; ?>
    </head>
    <body data-spy="scroll" data-target=".navbar" data-offset="90">

        <?php include 'inner-nav.php'; ?>
        <div style="background-color:#fffff;" >
            <div class="container" >
                <div class="row ">
                    <div class="col-lg-6 mt-2 " style="padding-top:100px;">
                        <h3 class="wow fadeInUp text-black" data-wow-delay="300" style="visibility: visible; animation-name: fadeInUp;">Mobile Apps</h3>
                        <div class="border border-success text-white"></div>
                        <h5 class="mt-3 wow fadeInUp" data-wow-delay="300ms" style="visibility: visible; animation-delay: 300ms; animation-name: fadeInUp;"><p class="text-justify">AR is one of the latest technologies which creates a bridge to connect the Physical World with Digital environment. Thoughtspheres (TS) is committed to bring this niche technology into limelight and turn it useful in gainful solutions. We are one of the few players in India who has expertise in this technology. TS have expert engineers on board who are dedicated to create cutting edge solutions for the customers over the world. TS is offering AR solutions on Android, IPhone and Nokia smart phones platform as well as AR for web enterprise applications.We hope to build ground breaking solutions on AR for airline, airport ,banking, real estate, education and healthcare sector.
                                Capabilities:</p>
                            Capabilities:</h5>
                        <ul class="pl-3 wow fadeInUp" data-wow-delay="600ms" style="visibility: visible; animation-delay: 600ms; animation-name: fadeInUp;">

                            <li>COE for the development of Image and Location Based AR applications</li>
                            <li>Expertise on AR for Android, IPhone and Windows smart phones</li>
                        </ul> 

                    </div>
                    <div class="col-lg-6 mt-5 pt-5 pb-5">
                        <img src="images/tech-mobility.png" class="img-fluid" style="margin-top: 30%;">
                    </div>
                </div>
            </div>
        </div>
        <!--header end-->
        <div class="clearfix"></div>      
        <?php include 'ourclient.php'; ?>
        <!-- address Start -->          
        <!-- Footer end -->
        <?php include 'footer.php'; ?>
    </body>


</html>