<!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
        <title>thoughtspheres</title>    
        <?php include 'header.php';?>
    </head>
    <body data-spy="scroll" data-target=".navbar" data-offset="90">
         <?php include 'inner-nav.php';?>
        <div style="background-color:#fffff;" >
            <div class="container" >
                <div class="row ">
                    <div class="col-lg-6 mt-2 " style="padding-top:100px;">
                        <h3 class="wow fadeInUp text-black" data-wow-delay="300" style="visibility: visible; animation-name: fadeInUp;">Smart UI Platform </h3>
                        <div class="border border-success text-white"></div>
                        <h5 class="mt-3 wow fadeInUp" data-wow-delay="300ms" style="visibility: visible; animation-delay: 300ms; animation-name: fadeInUp;"><p class="text-justify">Technology makes better sense when it is well designed. Smart UI thus plays a vital role in turning technology more usable ,worthy and attractive. TS has a competent and dedicated UI and UX team which helps enterprises reinvent noteworthy aspects of their organization. We possess excellent team adept at creating Rich Internet Application (RIA) and desktop application. This team interacts with the customer from the initial stages of the project to have a clear and precise understanding of their expectation while interacting with the application. Then this team emerges with a very light weight scalable solution to produce an awesome user experience.</p>
Capabilities:</h5>
                        <ul class="pl-3 wow fadeInUp" data-wow-delay="600ms" style="visibility: visible; animation-delay: 600ms; animation-name: fadeInUp;">
                            <li> Expertise in Reach Internet Applications(RIA)</li>
 <li>Expertise in Adobe Flex and GWT</li>
 <li> Strong knowledge of Adobe AIR development</li>
 <li> Expertise in AJAX and JQuery</li>
 <li> Good exposure to Tweeter Bootstrap</li>
 <li> Strong knowledge of HTML5, Sass , Coffe Script , Knockout , Node.js etc</li>                   
                </ul> 
                 </div>
                    <div class="col-lg-6 mt-5 pt-5 pb-5">
                        <img src="images/tech-mobility.png" class="img-fluid" style="margin-top: 30%;">
                    </div>
                </div>
            </div>
        </div>
        <!--header end-->
        <div class="clearfix"></div>
        <?php include 'ourclient.php'; ?>   
            <!-- address Start -->
                      <!-- Footer end -->
           <?php include 'footer.php';?>

    </body>


</html>