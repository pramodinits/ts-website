<?php include 'header.php'; ?>
<?php include 'inner-nav.php'; ?>
<div class="bigdata-bg">
    <div class="container">
        <div class="row fill-main">
            <div class="col-md-6"></div>
            <div class="col-md-6 col-sm-12 col-xs-12 text-center bigdata-header">
                <h2 class="wow fadeInUp text-uppercase" data-wow-delay="400ms" style="font-weight: 900; visibility: visible; animation-delay: 400ms; animation-name: fadeInUp;">
                    Big <span class="text-ray">Data</span>
                </h2>
                <div class="text-center">
                    <img class="img-fluid" src="images/gradient.png"/>
                </div>
                <div class="col-md-12 col-sm-12 col-xs-12 text-left">
                    <p>The exponential growth of mobile device adoption by the individuals and enterprises has made it an inevitable medium to bring humans, 
                        technology and security under one platform. Thus, a robust enterprise is obvious to seek mobile solutions for every business aspect 
                        which includes not only development of an attractive app rather integrated solutions to support key business transformation. TS offers 
                        cost effective unique mobile solutions to help organizations maximize their profit and streamline their processes.</p>
                </div>
            </div>
        </div>
        <div class="row fill-main features-bigdata">
            <div class="col-md-4"></div>
            <div class="col-md-8">
                <div class="col-md-12">
                    <h5><strong>Capabilities:</strong></h5>
                </div>
                <div class="col-md-12">
                    <ul>
                        <li>COE for Android, IPhone and Windows Apps Development</li>
                        <li>J2ME and S40 Series API Practices</li>
                        <li>Expertise in Hybrid Mobile Application technologies like PhoneGap, Xamarin etc</li>
                        <li>Expertise in HTML5, Ionic Framework, React, AngularJS etc</li>
                        <li>Mobile Apps Testing</li>
                        <li>Mobile Analytics</li>
                        <li>Expertise in Consumer and Enterprise Mobility</li>
                    </ul>
                </div>
            </div>
        </div>
    </div>
</div>
<div class="block-responsive">
    <div class="bigdata-bg-responsive"></div>
    <div class="container">
        <div class="row">
            <div class="col-sm-12 col-xs-12 text-center bigdata-header mt-3">
                <h2 class="wow fadeInUp text-uppercase" data-wow-delay="400ms" style="font-weight: 900; visibility: visible; animation-delay: 400ms; animation-name: fadeInUp;">
                    Big <span class="text-ray">Data</span>
                </h2>
                <div class="text-center">
                    <img class="img-fluid" src="images/gradient.png"/>
                </div>
                <div class="col-md-12 col-sm-12 col-xs-12 text-left">
                    <p>The exponential growth of mobile device adoption by the individuals and enterprises has made it an inevitable medium to bring humans, 
                        technology and security under one platform. Thus, a robust enterprise is obvious to seek mobile solutions for every business aspect 
                        which includes not only development of an attractive app rather integrated solutions to support key business transformation. TS offers 
                        cost effective unique mobile solutions to help organizations maximize their profit and streamline their processes.</p>
                </div>
            </div>
            <div class="mt-3">
                <div class="col-md-12">
                    <h5><strong>Capabilities:</strong></h5>
                </div>
                <div class="col-md-12">
                    <ul>
                        <li>COE for Android, IPhone and Windows Apps Development</li>
                        <li>J2ME and S40 Series API Practices</li>
                        <li>Expertise in Hybrid Mobile Application technologies like PhoneGap, Xamarin etc</li>
                        <li>Expertise in HTML5, Ionic Framework, React, AngularJS etc</li>
                        <li>Mobile Apps Testing</li>
                        <li>Mobile Analytics</li>
                        <li>Expertise in Consumer and Enterprise Mobility</li>
                    </ul>
                </div>
            </div>
        </div>
    </div>
</div>
<div class="clearfix mt-3"></div>
<div class="container" style="margin-top: 8%;">
    <div class="row">
        <div class="col-md-12">
            <p>
                The exponential growth of mobile device adoption by the individuals and enterprises has made it an inevitable medium to bring humans, 
                technology and security under one platform. Thus, a robust enterprise is obvious to seek mobile solutions for every business aspect 
                which includes not only development of an attractive app rather integrated solutions to support key business transformation. TS offers 
                cost effective unique mobile solutions to help organizations maximize their profit and streamline their processes.</p>
        </div>
    </div>
</div>
<?php include 'footer.php'; ?>