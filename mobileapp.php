<!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
        <title>thoughtspheres</title>    
        <?php include 'header.php'; ?>
    </head>
    <body data-spy="scroll" data-target=".navbar" data-offset="90">
        <?php include 'inner-nav.php'; ?>
        <div class="mobilebg">         
        </div>
        <div style="background-color:#fffff;" >
            <div class="container" >
                <div class="row ">
                    <div class="col-lg-6 mt-2  text-center" style="padding-top:100px;">
                        <h3 class="wow fadeInUp text-uppercase" data-wow-delay="400ms" style="font-weight: 900; visibility: visible; animation-delay: 400ms; animation-name: fadeInUp;">Mobile <span class="text-ray">Apps</span></h3>
                        <div class="ctm-border"></div>
                        <h5 class="text-center mt-4 wow fadeInUp" data-wow-delay="300ms" style="visibility: visible; animation-delay: 300ms; animation-name: fadeInUp;"><b>Let Ts guide (Consulting), develop & Launch it for you</b> </h5>
                        <h3 class="mt-5 text-success">Why TS ?</h3>                       
                        <h5 class="text-center  wow fadeInUp" data-wow-delay="300ms" style="visibility: visible; animation-delay: 300ms; animation-name: fadeInUp;"><b>As it’s a easy job for us across all Platform</b> </h5>
                        <div class="btn-group text-center mt-4" role="group" aria-label="Basic example">
                            <button type="button" class="btn btn-naviblue btn-radius btn-lg">Android</button>
                            <button type="button" class="btn btn-success btn-radius btn-lg ml-4 mr-4 pl-4 pr-4">IOS</button>
                            <button type="button" class="btn btn-naviblue btn-radius btn-lg">Hybrid</button>
                        </div>
                    </div>
                    <div class="col-lg-6 mt-5 pt-5 pb-5">
                        <img src="images/mobile-apps.png" class="img-fluid">
                    </div>
                    <div class="row">
                        <div class="col-md-12">
                            <p class="text-justify">The exponential growth of mobile device adoption by the individuals and enterprises has made it an inevitable medium to bring humans, technology and security under one platform. Thus, a robust enterprise is obvious to seek mobile solutions for every business aspect which includes not only development of an attractive app rather integrated solutions to support key business transformation. TS offers cost effective unique mobile solutions to help organizations maximize their profit and streamline their processes.</p>
                            <p class="font-weight-bold">Capabilities:</p>
<ul class="pl-3 wow fadeInUp" data-wow-delay="600ms" style="visibility: visible; animation-delay: 600ms; animation-name: fadeInUp;">

                            <li>COE for the development of Image and Location Based AR applications</li>
                            <li>Expertise on AR for Android, IPhone and Windows smart phones</li>
                            <li>COE for Android, IPhone and Windows Apps Development</li>
                            <li>J2ME and S40 Series API Practices</li>
                            <li>Expertise in Hybrid Mobile Application technologies like PhoneGap, Xamarin etc</li>
                            <li>Expertise in HTML5, Ionic Framework, React, AngularJS etc</li>
                            <li>Concept to Design and Development of Mobile Apps and Games</li>
                            <li>Mobile Apps Testing</li>
                            <li>Mobile Analytics</li>
                            <li>Expertise in Consumer and Enterprise Mobility</li>
                            <li>Expertise in mobile payment solutions, retail and healthcare solutions</li>
                        </ul>
                      
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <!--header end-->
        <div class="clearfix"></div>      
        <?php include 'ourclient.php'; ?>      
        <?php include 'footer.php'; ?>
    </body>
</html>