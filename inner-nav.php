<body data-spy="scroll" data-target=".navbar" data-offset="90">
    <header id="home" class="">
        <nav class="navbar navbar-top navbar-expand-lg side-logo-nav nav-line bg-green">
            <div class="container-fluid">
                <a href="index.php" title="Logo" class="logo">
                    <img src="images/logo.png" class="logo-dark" alt="logo">
                    <img src="images/logo.png" alt="logo" class="logo-light default">
                </a>
                <div class="" id="navbarTogglerDemo">
                    <ul class="navbar-nav mx-auto mt-lg-0 ">
                        <li class="nav-item active">
                            <a class="nav-link" href="index.php">Home <span class="sr-only">(current)</span></a>
                        </li>
                        <li class="nav-item dropdown">
                            <a class="nav-link dropbtn" href="about.php">About Us</a>
                            <ul class="dropdown-content">  
                                <li><a href="about.php#wt-we-are" class="text-black">Who We Are</a></li>
                                <li><a href="about.php#mission" class="text-black">Mission & Vision</a></li>
                                <li><a href="about.php#our-team" class="text-black">Our Team</a></li>
                            </ul>                        
                        </li>
                        <li class="nav-item dropdown">
                            <a class="nav-link dropbtn">Technologies</a>
                            <ul class="dropdown-content">    
                                <li><a href="ai-ml.php" class="text-black">AI/ML</a></li>
                                <li><a href="bigdata.php" class="text-black">Big Data</a></li>
                                <li><a href="cyber-security.php" class="text-black">Cyber Security</a></li>                            
                                <li><a href="ar-vr.php" class="text-black">AR/VR</a></li>
                            </ul>
                        </li>
                        <li class="nav-item dropdown">
                            <a class="nav-link dropbtn" href="news.php">Resources</a>
                            <ul class="dropdown-content">
                                <li><a href="news.php" class="text-black">News</a></li>
                                <li><a href="blog.php" class="text-black">Blog</a></li>
                            </ul>
                        </li>
                        <li class="nav-item">
                            <a class="nav-link" href="career.php">Careers</a>
                        </li>
                        <li class="nav-item mt-2">
                            <a href="contactus.php" class="btn custom-btn2 nav-link" data-wow-delay="700ms" style="visibility: visible; animation-delay: 700ms; animation-name: fadeInUp;">Get In Touch</a>
                        </li>
                    </ul>
                </div>
                <div class="social_icons">
                    <ul>
                        <li><a href="https://www.facebook.com/thoughtsSpheres/" class="facebook_bg_hvr2"><i class="fa fa-facebook" aria-hidden="true"></i></a> </li>
                        <li><a href="https://www.linkedin.com/company/thoughtspheres-technologies-pvt-ltd-/" class="instagram_bg_hvr2"><i class="fa fa-linkedin" aria-hidden="true"></i></a> </li>
                        <li><a href="#." class="twitter_bg_hvr2"><i class="fa fa-twitter" aria-hidden="true"></i></a> </li>
                    </ul>
                </div>
            </div>
        </nav>
        <!--mobile menu start-->
        <nav class="navbar navbar-expand-lg navbar-light mobilenav bg-green mobilenav">
            <a href="index.php" title="Logo" class="logo">
                <img src="images/logo.png" class="logo-dark img-fluid" alt="logo">
                <img src="images/logo.png" alt="logo" class="logo-light default img-fluid">
            </a>
            <button class="navbar-toggler pull-right" data-toggle="collapse" data-target="#navbarTogglerDemo02" aria-controls="navbarTogglerDemo02" aria-expanded="false" aria-label="Toggle navigation">
                <span><i class="fa fa-bars"></i></span>
            </button>
            <div class="collapse navbar-collapse" id="navbarTogglerDemo02">
                <ul class="navbar-nav mr-auto mt-lg-0">
                    <li class="nav-item active">
                        <a class="nav-link" href="index.php">Home <span class="sr-only">(current)</span></a>
                    </li>
                    <li class="nav-item dropdown">
                        <a class="nav-link" href="about.php">About Us</a>
                    </li>
                    <li class="nav-item dropdown">
                        <input id="check01" type="checkbox" name="menu"/>
                        <label for="check01"><a class="nav-link dropbtn">Technologies</a></label>
                        <ul class="submenu">
                            <li><a href="ai-ml.php" class="text-black">AI/ML</a></li>
                            <li><a href="bigdata.php" class="text-black">Big Data</a></li>
                            <li><a href="cyber-security.php" class="text-black">Cyber Security</a></li>                            
                            <li><a href="ar-vr.php" class="text-black">AR/VR</a></li>
                        </ul>
                    </li>
                    <li class="nav-item dropdown">
                        <input id="check02" type="checkbox" name="menu"/>
                        <label for="check02"><a class="nav-link dropbtn">Resources</a></label>
                        <ul class="submenu">
                            <li><a href="news.php" class="text-black">News</a></li>
                            <li><a href="blog.php" class="text-black">Blog</a></li>                                 
                        </ul>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link" href="career.php">Careers</a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link " href="contactus.php">Get In Touch</a>
                    </li>
                </ul>
            </div>
        </nav>
        <!--mobile menu end-->
    </header>
