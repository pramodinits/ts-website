<?php include 'header.php' ?>
<?php include 'inner-nav.php' ?>
<div class="ar-vrbg">     
    <div class="container">
        <div class="row fill-main features-rightbanner">
            <div class="col-lg-6 col-xs-12 col-sm-12 text-center">
                <h3 class="wow fadeInUp text-uppercase" data-wow-delay="400ms" style="font-weight: 900; visibility: visible; animation-delay: 400ms; animation-name: fadeInUp;">AR <span class="text-ray">/ VR</span></h3>
                <div class="text-center">
                    <img class="img-fluid" src="images/gradient.png"/>
                </div>
                <p class="text-left">
                    AR is one of the latest technologies which creates a bridge to connect the Physical World with Digital environment. Thoughtspheres (TS) is committed to bring this niche technology into 
                    limelight and turn it useful in gainful solutions. We are one of the few players in India who has expertise in this technology. TS have expert engineers on board who are dedicated to create cutting edge solutions 
                    for the customers over the world.TS is offering AR solutions on Android, IPhone and Nokia smart phones platform as well as AR for web enterprise applications.We hope to build ground breaking
                    solutions on AR for airline, airport ,banking, real estate, education and healthcare sector.
                </p>
            </div>
        </div>
    </div>
</div>
<div class="block-responsive">
    <div class="ar-vrbg-responsive mb-3"></div>
    <div class="container">
        <div class="row">
            <div class="col-lg-6 col-xs-12 col-sm-12 text-center">
                <h3 class="wow fadeInUp text-uppercase" data-wow-delay="400ms" style="font-weight: 900; visibility: visible; animation-delay: 400ms; animation-name: fadeInUp;">AR <span class="text-ray">/ VR</span></h3>
                <div class="text-center">
                    <img class="img-fluid" src="images/gradient.png"/>
                </div>
                <p class="text-left">
                    AR is one of the latest technologies which creates a bridge to connect the Physical World with Digital environment. Thoughtspheres (TS) is committed to bring this niche technology into 
                    limelight and turn it useful in gainful solutions. We are one of the few players in India who has expertise in this technology. TS have expert engineers on board who are dedicated to create cutting edge solutions 
                    for the customers over the world.TS is offering AR solutions on Android, IPhone and Nokia smart phones platform as well as AR for web enterprise applications.We hope to build ground breaking
                    solutions on AR for airline, airport ,banking, real estate, education and healthcare sector.
                </p>
            </div>
        </div>
    </div>
</div>
<div class="container">
    <div class="row">
        <div class="col-md-12 col-xs-12 col-sm-12">
            <p class="font-weight-bold">Capabilities:</p>
            <ul class="pl-3">
                <li>COE for the development of Image and Location Based AR applications</li>
                <li>Expertise on AR for Android, IPhone and Windows smart phones</li>
            </ul>                      
        </div>
    </div>
    <div class="row">
        <div class="col-md-12">
            <p>
                VR is a digitally created artificial environment which is so powerfully designed that user almost believes and accepts 
                it as a real environment/experience. VR is an immediate crowd puller as it is a true immersive experience-it can 
                serve as a transporting medium to an adventurous /unreachable place without having to spend or waste effort to be there. One feels as if he/she is right there on the spot, in the middle of the action. Thoughtspheres possesses this trailblazing 
                technical competency thus hoping to be the trailbrazer in the IT arena.
            </p>
        </div>
    </div>
</div>

<!--header end-->
<div class="clearfix"></div>      
<?php include 'footer.php'; ?>