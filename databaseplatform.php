<!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
        <title>thoughtspheres</title>    
        <?php include 'header.php'; ?>
    </head>
    <body data-spy="scroll" data-target=".navbar" data-offset="90">
        <?php include 'inner-nav.php'; ?>
        <div style="background-color:#fffff;" >
            <div class="container" >
                <div class="row ">
                    <div class="col-lg-6 mt-2 " style="padding-top:100px;">
                        <h3 class="wow fadeInUp text-black" data-wow-delay="300" style="visibility: visible; animation-name: fadeInUp;">AI </h3>
                        <div class="border border-success text-white"></div>
                        <h5 class="mt-3 wow fadeInUp" data-wow-delay="300ms" style="visibility: visible; animation-delay: 300ms; animation-name: fadeInUp;">
                            <p class="text-justify">Any business application in the world depends on Database technology. The relational DBMS is the modern base platform that uses as backend to store data and contents for any enterprise applications. TS has a highly experienced team in this vital domain with wide exposure in various Database Technology stacks. TS does provide data driven strategic insights to customers as well as develop scalable and robust Database solutions to meet high-performance applications.</p>
                            Capabilities:</h5>
                        <ul class="pl-3 wow fadeInUp" data-wow-delay="600ms" style="visibility: visible; animation-delay: 600ms; animation-name: fadeInUp;">
                            <li>Database Support Database Architecture and Design</li>
                            <li>Data Modeling</li>
                            <li> Performance Tuning</li>
                            <li>Database Administration</li>
                        </ul> 
                    </div>
                    <div class="col-lg-6 mt-5 pt-5 pb-5">
                        <img src="images/tech-mobility.png" class="img-fluid" style="margin-top: 30%;">
                    </div>
                </div>
            </div>
        </div>
        <!--header end-->
        <div class="clearfix"></div>
        <?php include 'ourclient.php'; ?>
        <?php include 'footer.php'; ?>
    </body>
</html>