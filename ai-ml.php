<?php include 'header.php'; ?>
<?php include 'inner-nav.php'; ?>
<div class="aiml-banner">
    <img class="img-fluid aiml-bg" src="images/ai.jpg">
    <img class="img-fluid block-responsive" src="images/ai_mobile.jpg">
</div>
<div class="container">
    <div class="row">
        <div class="col-lg-12 mt-6 text-center mt-3">
            <h2 class="wow fadeInUp text-black font-weight-bold" data-wow-delay="300" style="visibility: visible; animation-name: fadeInUp;">AI/<span class="text-ray">ML</span></h2>
            <div class="text-center">
                <img class="img-fluid" src="images/gradient.png"/>
            </div>
        </div>
    </div>
    <div class="clearfix"></div>
    <div class="row mt-3">
        <div class="col-md-12">
            <p>Artificial intelligence (AI) is the most sought after technologies which is instrumental in creating smart devices which can execute tasks which generally require human intelligence; learn, reason, plan, perceive, or process natural language.AI can be used in most sectors by processing rich data and pattern recognition in the data.It can perform tasks with great accuracy through progressive learning algorithms. At ThoughtSpheres, we have a team of high-spiriteded experts who excel at Narrow AI which is characterized by development in machine learning and deep learning. </p>
        </div>
    </div>
    <div class="row mt-4">
        <div class="features-header col-md-12">
            <h5><strong>Capabilities:</strong></h5>
        </div>
        <div class="aiml-features col-md-12">
            <ul>
                <li>COE for Android, IPhone and Windows Apps Development</li>
                <li>J2ME and S40 Series API Practices</li>
                <li>Expertise in Hybrid Mobile Application technologies like PhoneGap, Xamarin etc</li>
                <li>Expertise in HTML5, Ionic Framework, React, AngularJS etc</li>
                <li>Concept to Design and Development of Mobile Apps and Games</li>
                <li>Mobile Apps Testing</li>
                <li>Mobile Analytics</li>
                <li>Expertise in Consumer and Enterprise Mobility</li>
                <li>Expertise in mobile payment solutions, retail and healthcare solutions</li>
            </ul>
        </div>
    </div>
</div>
<?php include 'footer.php'; ?>