<?php include 'header.php'; ?>
<?php include 'inner-nav.php'; ?>
<div class="clearfix"></div>
<div class="tp-bgimg">
    <div class="container-fluid">
        <div class="row fill-main">
            <div class="col-lg-6 text-center">
                <h3 class="wow fadeInUp text-uppercase transparent-bghead" data-wow-delay="400ms">Product <span class="text-ray">Engineering</span></h3>
                <div class="ctm-border"></div>
                <h5 class="text-center mt-4 wow fadeInUp" data-wow-delay="300ms" style="visibility: visible; animation-delay: 300ms; animation-name: fadeInUp;">
                    <strong>
                        Crafting innovation through cutting edge technology
                    </strong> 
                </h5>
            </div>
            <div class="col-lg-6" style="background-color: #771bf0!important; opacity: .8;">
                <p class="text-white text-justify " style="padding: 25% 10%;">
                    At ThoughtSpheres, we take pride in creating ground-breaking products for Enterprise as well as Consumers. Our Product Engineering Team demonstrates next generation technology skills. Deep understanding of critical business processes and consumer behavior allows us to offer best possible platform/solutions with extraordinary ease. Our core competence lies in the following areas: Mobility, Cloud, Social Solutions & Analytics.<br>
                    We undertake outsourced product design and development. Adopting a strictly professional approach TS collaborates with clients to build products across multiple domains to help them attain competitive advantage.
                </p>
            </div>
        </div>
    </div>
</div>
<div class="container-fluid">
    <div class="row fill-main business-body">
        <div class="col-lg-12 mt-5 text-center">
            <img src="images/product-engineering.jpg" class="img-fluid " alt="">
        </div>
    </div>
</div>
<?php include 'footer.php'; ?>