<?php include 'header.php'; ?>
<?php include 'inner-nav.php'; ?>
<div class="clearfix"></div>
<div class="tp-bgimg">
    <div class="container-fluid">
        <div class="row fill-main">
            <div class="col-lg-6 text-center">
                <h3 class="wow fadeInUp text-uppercase transparent-bghead" data-wow-delay="400ms">Digital <span class="text-ray">Services</span></h3>
                <div class="ctm-border"></div>
                <h5 class="text-center mt-4 wow fadeInUp" data-wow-delay="300ms" style="visibility: visible; animation-delay: 300ms; animation-name: fadeInUp;">
                    <strong>
                        Develop, Disrupt, Disperse with Digital
                    </strong> 
                </h5>
            </div>
            <div class="col-lg-6" style="background-color: #771bf0!important; opacity: .8;">
                <p class="text-white text-justify" style="padding: 24% 10%;">
                    TS Digital Services is comprised of TS Synergy,TS Mobility and TS Analytics which aim at extending seamless consumers & enterprise experience. TS Synergy provides a comprehensive marketing solution ranging from creative design to digital campaigns utilizing our expertise in the field of content and social media to e-learning packages for enhanced customer engagement and consequently business generation. TS Mobility claims exquisite authority,having created Innovative Mobility solutions for numerous clients.We have expertise in the niche technology called AR . TS Analytics team possesses superior dexterity in helping the enterprise enjoy leading market position through critical data insights and analysis.
                </p>
            </div>
        </div>
    </div>
</div>
<div class="container-fluid">
    <div class="row fill-main business-body">
        <div class="col-lg-12 mt-3 text-center">
            <img src="images/digital-services.jpg" class="img-fluid " alt="">
        </div>
    </div>
</div>
<?php include 'footer.php'; ?>