<!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
        <title>thoughtspheres</title>    
        <?php include 'header.php'; ?>
    </head>
    <body data-spy="scroll" data-target=".navbar" data-offset="90">
        <?php include 'inner-nav.php'; ?>
        <div style="background-color:#fffff;" >
            <div class="container" >
                <div class="row ">
                    <div class="col-lg-6 mt-2 " style="padding-top:100px;">
                        <h3 class="wow fadeInUp text-black" data-wow-delay="300" style="visibility: visible; animation-name: fadeInUp;">Java/JEE Platform</h3>
                        <div class="border border-success text-white"></div>
                        <h5 class="mt-3 wow fadeInUp" data-wow-delay="300ms" style="visibility: visible; animation-delay: 300ms; animation-name: fadeInUp;"><p class="text-justify">We have a very strong and dedicated team in Java/Java EE platform. Wrapped with huge experience to handle complicated Enterprise Web Applications / Products under Java EE platform,TS Java team is focused on coding standards, architecture, design patterns and reusable frameworks owning excellent skills working in weblogic, websphere, Sun JavaEE, JBoss and Tomcat server..</p>
                            Capabilities:</h5>
                        <li> COE for java/Java EE Technologies and related open source frameworks</li>
                        <li> Expertise in Java EE components like Servlet, JSP and EJB etc</li>
                        <li> Strong Skills in JSF framework</li>
                        <li> Expertise in third party open source frameworks like Struts, Spring and Hibernate</li>
                        <li> Strong understanding of Java EE architecture and design patterns</li>
                        <li> Expertise in OS GIS , SOA and Web Services</li>
                        <li> Expertise in Agile Development Methodologies</li>

                    </div>
                    <div class="col-lg-6 mt-5 pt-5 pb-5">
                        <img src="images/tech-mobility.png" class="img-fluid" style="margin-top: 30%;">
                    </div>
                </div>
            </div>
        </div>
        <!--header end-->
        <div class="clearfix"></div>
          <?php include 'ourclient.php'; ?>        
        <?php include 'footer.php'; ?>
    </body>


</html>