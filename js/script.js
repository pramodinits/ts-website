var swiper = new Swiper('.main-slider-header', {
    parallax: true,
});

$('#type_it_slider').typeIt({
    speed: 100,
    autoStart: false,
    loop: true,
})
    .tiType('<span style="color:#00d8b4; font-weight:bold">ThoughtSpheres</span>')
    .tiSettings({speed: 700})
    .tiPause(1000)
    .tiSettings({speed: 50})
    .tiDelete()
     .tiType('<span style="color:#00d8b4; font-weight:bold">ThoughtSpheres</span>')
    .tiSettings({speed: 700})
    .tiPause(1000)
    .tiSettings({speed: 50})
    .tiDelete()
    .tiType('<span style="color:#00d8b4; font-weight:bold">ThoughtSpheres</span>')


$(".scroll").on('click', function(event) {
    event.preventDefault();
    $('html,body').animate({ scrollTop: $(this.hash).offset().top }, 1000);
});

//Wow Initializing
new WOW().init();

window.onload = function() {
    Particles.init({
        selector: '.background_3',
        color: '#ffffff',
        connectParticles: true,
        sizeVariations: 9,
        maxParticles: 200,
    });
};